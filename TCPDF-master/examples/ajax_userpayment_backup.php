<?php

$HostName = $_SERVER['HTTP_HOST'];
if($HostName == 'localhost' ){
	 require_once("../../connection.php"); 
}else{
	 require_once("../../connection.php"); 
}

$paymentmessage = '';
$mess_sqlInser = '';


if( isset($_POST['currency_job_input_name']) && (!empty($_POST['currency_job_input_name']))){
		$currency_job_input_name =  $_POST['currency_job_input_name'];
		$currency_job_input =  $_POST['currency_job_input'];
		$curr_curency_value =  $_POST['curr_curency_value'];
		$service_job_input_name =  $_POST['service_job_input_name'];
		$service_job_input =  $_POST['service_job_input'];
		$source_lang_job_input_name =  $_POST['source_lang_job_input_name'];
		$source_lang_job_input =  $_POST['source_lang_job_input'];
		$target_lang_job_input_name =  $_POST['target_lang_job_input_name'];
		$target_lang_job_input =  $_POST['target_lang_job_input'];

		$specialised_job_input_name =  $_POST['specialised_job_input_name'];
		$specialised_job_input =  $_POST['specialised_job_input'];
		$specialised_input_percent =  $_POST['specialised_input_percent'];
		$specialised_text =  $_POST['specialised_text'];

		$quality_job_input_name =  $_POST['quality_job_input_name'];
		$quality_job_input =  $_POST['quality_job_input'];
		$quality_input_percent =  $_POST['quality_input_percent'];
		$quality_text =  $_POST['quality_text'];

		$certification_job_input_name =  $_POST['certification_job_input_name'];
		$certification_job_input =  $_POST['certification_job_input'];
		$certification_input_percent =  $_POST['certification_input_percent'];
		$certification_text =  $_POST['certification_text'];

		$measure_input_name =  $_POST['measure_input_name'];
		$measure_input =  $_POST['measure_input'];
		$quantity_input =  $_POST['quantity_input'];
		$TotalSUM_base_price_input =  $_POST['TotalSUM_base_price_input'];
		$TotalSUM_certification_input =  $_POST['TotalSUM_certification_input'];
		$TotalSUM_domain_input =  $_POST['TotalSUM_domain_input'];
		$TotalSUM_quality_input =  $_POST['TotalSUM_quality_input'];
		$NetTotal_input =  $_POST['NetTotal_input'];
		$add_date_time = date('Y-m-d H:i:s');
		$id = $_SERVER['REMOTE_ADDR'];
		$user_id = $_POST['user_login_id'];
		$user_agent = get_browser_name($_SERVER['HTTP_USER_AGENT']);

		$sqlInser = "INSERT INTO tbl_order SET service_id = '$service_job_input',  	service_name = '$service_job_input_name', user_id = '$user_id', currency_code = '$currency_job_input', currency_value = '$curr_curency_value', source_lang_name = '$source_lang_job_input_name', source_lang_id = '$source_lang_job_input', target_lang_name = '$target_lang_job_input_name', target_lang_id = '$target_lang_job_input', domian_name = '$specialised_job_input_name', domain_id = '$specialised_job_input', specialised_input_percent = '$specialised_input_percent' , domain_text = '$specialised_text' , quality_name = '$quality_job_input_name', quality_id = '$quality_job_input', quality_input_percent = '$quality_input_percent' , quality_text = '$quality_text' ,certification_name = '$certification_job_input_name', certification_id = '$certification_job_input', certification_input_percent = '$certification_input_percent' , certification_text = '$certification_text',  measure_name = '$measure_input_name', measure_id = '$measure_input', quantity = '$quantity_input', totalSUM_base_price = '$TotalSUM_base_price_input', totalSUM_certification = '$TotalSUM_certification_input', totalSUM_domain = '$TotalSUM_domain_input', totalSUM_quality = '$TotalSUM_quality_input', netTotal = '$NetTotal_input', order_status_id = '0', ip = '$id', user_agent = '$user_agent', added_date_time = NOW()  ";
        
		$result = mysql_query($sqlInser);

		$mess_sqlInser = $sqlInser;
		$num_rows = mysql_affected_rows(); 
		if( mysql_affected_rows() > 0){

			$new_order_id = mysql_insert_id();
			$paymentmessage = "<div class='success' >Order submitted successfully.</div>";			
			
		}else{
			$paymentmessage = "<div class='warning' >Order could not add, try again.</div>";
			

		}
}else{
  $paymentmessage = "<div class='warning' >Order could not add, try again.</div>";
}

function get_browser_name($user_agent)
{
    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
    elseif (strpos($user_agent, 'Edge')) return 'Edge';
    elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
    elseif (strpos($user_agent, 'Safari')) return 'Safari';
    elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
    elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';
    
    return 'Other';
}


$data = array(
   "paymentmessage" => $paymentmessage,
   "mess_sqlInser"	=> $mess_sqlInser
);



if( isset($new_order_id)){

/// Start to pdf		----------------------------------------------------------------------------
// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 009');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 009', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// -------------------------------------------------------------------

// add a page
$pdf->AddPage();

// set JPEG quality
$pdf->setJPEGQuality(75);

// Image method signature:
// Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// Example of Image from data stream ('PHP rules')
$imgdata = base64_decode('iVBORw0KGgoAAAANSUhEUgAAABwAAAASCAMAAAB/2U7WAAAABlBMVEUAAAD///+l2Z/dAAAASUlEQVR4XqWQUQoAIAxC2/0vXZDrEX4IJTRkb7lobNUStXsB0jIXIAMSsQnWlsV+wULF4Avk9fLq2r8a5HSE35Q3eO2XP1A1wQkZSgETvDtKdQAAAABJRU5ErkJggg==');

// The '@' character is used to indicate that follows an image data stream and not an image file name
$pdf->Image('@'.$imgdata);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// Image example with resizing
$pdf->Image('images/image_demo.jpg', 15, 140, 75, 113, 'JPG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 1, false, false, false);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// test fitbox with all alignment combinations

$horizontal_alignments = array('L', 'C', 'R');
$vertical_alignments = array('T', 'M', 'B');

$x = 15;
$y = 35;
$w = 30;
$h = 30;
// test all combinations of alignments
for ($i = 0; $i < 3; ++$i) {
	$fitbox = $horizontal_alignments[$i].' ';
	$x = 15;
	for ($j = 0; $j < 3; ++$j) {
		$fitbox[1] = $vertical_alignments[$j];
		$pdf->Rect($x, $y, $w, $h, 'F', array(), array(128,255,128));
		$pdf->Image('images/image_demo.jpg', $x, $y, $w, $h, 'JPG', '', '', false, 300, '', false, false, 0, $fitbox, false, false);
		$x += 32; // new column
	}
	$y += 32; // new row
}

$x = 115;
$y = 35;
$w = 25;
$h = 50;
for ($i = 0; $i < 3; ++$i) {
	$fitbox = $horizontal_alignments[$i].' ';
	$x = 115;
	for ($j = 0; $j < 3; ++$j) {
		$fitbox[1] = $vertical_alignments[$j];
		$pdf->Rect($x, $y, $w, $h, 'F', array(), array(128,255,255));
		$pdf->Image('images/image_demo.jpg', $x, $y, $w, $h, 'JPG', '', '', false, 300, '', false, false, 0, $fitbox, false, false);
		$x += 27; // new column
	}
	$y += 52; // new row
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// Stretching, position and alignment example

$pdf->SetXY(110, 200);
$pdf->Image('images/image_demo.jpg', '', '', 40, 40, '', '', 'T', false, 300, '', false, false, 1, false, false, false);
$pdf->Image('images/image_demo.jpg', '', '', 40, 40, '', '', '', false, 300, '', false, false, 1, false, false, false);

// -------------------------------------------------------------------

//Close and output PDF document
//$pdf->Output('example_009.pdf', 'I');

// $pdf->Output('C:/xampp/htdocs/xyz/fileimg.pdf', 'F');
$pdf_file_name =  'trans_order_'.$new_order_id.".pdf";
$pdf->Output('C:/xampp/htdocs/shantranslation/customer_order_pdf/'.$pdf_file_name, 'F');

// End to pdf			----------------------------------------------------------------------------
}

echo json_encode($data);
exit;
?>
