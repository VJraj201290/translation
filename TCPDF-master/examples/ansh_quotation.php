<?php
//============================================================+
// File name   : example_002.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 002 for TCPDF class
//               Removing Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Removing Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Shantranslation Quotation');
$pdf->SetTitle('Shantranslation Quotation');
$pdf->SetSubject('Shantranslation Email Quotation');
$pdf->SetKeywords('Shantranslation Quotation');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$PDF_MARGIN_LEFT = 15;
$PDF_MARGIN_TOP = 70;
$PDF_MARGIN_RIGHT = 15;
//echo PDF_MARGIN_LEFT.'---------'.PDF_MARGIN_TOP.'---------'. PDF_MARGIN_RIGHT ; exit;

$pdf->SetMargins($PDF_MARGIN_LEFT, $PDF_MARGIN_TOP, $PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
//$pdf->SetFont('times', 'BI', 20);
$pdf->SetFont('helvetica', '', 12);


// add a page


// set some text to print

$pdf->Image('images/banner_header.jpg', 15, 0, 180, 50, 'JPG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 0, false, false, false);

$html = '<table cellpadding="4" cellspacing="0" bgcolor="#0092CD" style="padding-left:20px;color:#FFFFFF;" >
 <tr>
  <td  align="center"  bgcolor="#000000" ><b>Quotation</b></td>
  <td  align="center"  bgcolor="#000000" ><b>Detail</b></td>
 </tr>
 <tr>
  <td  align="left" >&nbsp;&nbsp;&nbsp;Email Address</td>
  <td  align="center"  align="left" >&nbsp;&nbsp;&nbsp;  '.$PDF_MARGIN_RIGHT.' .......</td>
 </tr>
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Order ID</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Currency</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Service Type</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
  <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Source Language</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Target Language</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Expertise</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Specialised Charges</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Quality</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Quality Charges</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Certification</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Certification Charges</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr> 
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Measure</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Quantity</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Rate per unit</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Delivery In</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
 <tr>
  <td    align="left" >&nbsp;&nbsp;&nbsp;Total Cost</td>
  <td    align="left" >&nbsp;&nbsp;&nbsp;   ........</td>
 </tr>
</table>';

// output the HTML content
 $pdf->writeHTML($html, true, false, true, false, '');
//$pdf->SetXY(110, 200);
//$pdf->Image('images/banner_footer.jpg', 15, 0, 180, 55, 'JPG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 0, false, false, false);

$pdf->SetXY(15, 212);
$pdf->Image('images/banner_footer.jpg', '', '', 180, 50, '', '', 'T', false, 320, '', false, false, 0, false, false, false);
$pdf->AddPage();
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_002.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
