		<div class="col-lg-12 col-md-12 pull-left">
				<div class="mid-sec1">
				<div class="ser-bg">
						<div class="panel with-nav-tabs panel-default">
			
			<div class="panel-heading" style="background-color:#f5f5f5;">
					<ul class="nav nav-tabs">

					<?php if( $tabFlagTL == 'active') { ?>
					<li class="active" ><a href="#tab_translation_default" data-toggle="tab">Translation</a></li>
					<?php }else{ ?>
					<li><a href="#tab_translation_default" data-toggle="tab">Translation</a></li>
					<?php } ?>

					<?php if( $tabFlagTP == 'active') { ?>
					<li class="active" ><a href="#tab_transcription_default" data-toggle="tab">Transcription</a></li>
					<?php }else{ ?>
					<li ><a href="#tab_transcription_default" data-toggle="tab">Transcription</a></li>
					<?php } ?>


					<?php if( $tabFlagST == 'active') { ?>
					<li class="active" ><a href="#tab_sub_titling_default" data-toggle="tab">Sub Titling</a></li>
					<?php }else{ ?>
					<li><a href="#tab_sub_titling_default" data-toggle="tab">Sub Titling</a></li>
					<?php } ?>


					<?php if( $tabFlagPF == 'active') { ?>
					<li class="active" ><a href="#tab_proofreading_default" data-toggle="tab">Proofreading</a></li>
					<?php }else{ ?>
					<li><a href="#tab_proofreading_default" data-toggle="tab">Proofreading</a></li>
					<?php } ?>


					<?php if( $tabFlagTY == 'active') { ?>
					<li class="active" ><a href="#tab_typing_default" data-toggle="tab">Typing</a></li>
					<?php }else{ ?>
					<li><a href="#tab_typing_default" data-toggle="tab">Typing</a></li>
					<?php } ?>

						<li><a href="#tab_service_setting_default" data-toggle="tab">Service Setting</a></li>
						
					</ul>
			</div>
						
			<div class="panel-body" style="background-color:#f5f5f5;">
				<div class="tab-content">				
					<?php require_once(ABSPATH.'back_end/translation_toggle.php'); ?>
					<?php require_once(ABSPATH.'back_end/transcription_toggle.php'); ?>	
					<?php require_once(ABSPATH.'back_end/sub_titling_toggle.php'); ?>
					<?php require_once(ABSPATH.'back_end/proofreading_toggle.php'); ?>
					<?php require_once(ABSPATH.'back_end/typing_toggle.php'); ?>
					<?php require_once(ABSPATH.'back_end/service_setting_toggle.php'); ?>				
				</div>
			</div>
		</div>
					</div>
			   
				</div>
		</div>
		