<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<title>Shantranslation</title>
</head>




<body>
	<header>
	<div class="container-fluid hed-top">
    	<div class="container">
        	<div class="logo"><img src="<?php echo SUB_DIR; ?>/images/shan-logo1.png" border="0" alt="" class="img-responsive" /></div>
        </div>
    </div>
    
  </header>  
  	<div class="clr"></div>
  <section>
	<div class="container">
		<form  method="post">
            <div class="login-main">
                               
                              <div class="form-group">
                                <label for="exampleInputEmail1">User ID</label>
                                <input type="text" class="form-control" id="user" name="user" placeholder="User ID">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
								<input type="hidden" class="form-control" id="submitLogin" name="submitLogin" value="yes" >
                              </div>
                              
                              <?php if(isset($_SESSION['message'])){ echo $_SESSION['message']; echo "<br/>"; unset($_SESSION['message']);  } ?> 
                              <button type="submit" class="btn sider-btn">Login</button>
                           
            </div>
        </form>
    </div>
    </section>
    <div class="clr"></div>
    
    <footer>
    	<div class="container-fluid hed-foot">
    	<div class="container">
        	Copyright � 2016 | Privacy Policy
        </div>
    </div>
    
    </footer>
</body>
</html>
