<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php   include("./connection.php");

	if(!isset($_SESSION['SESS_id']))
	{
		$siteHead = site_url.'/index.php';
		header("Location: ".$siteHead);
		exit;
	}else if( $_SESSION['SESS_user_type'] != 'A'){

		$siteHead = site_url.'/index.php';
		header("Location: ".$siteHead);
		exit;
	
	}

?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- <script type="text/javascript" src="<?php echo site_url; ?>/js/jquery-3.1.0.min.js"></script> -->
<script type="text/javascript" src="<?php echo site_url; ?>/js/jQueryTab.js"></script>
<script type="text/javascript" src="<?php echo site_url; ?>/js/bootstrap.min.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/tooltip.css" rel="stylesheet" type="text/css" />

<title>Shantranslation</title>

<script src="js/jquery-1.9.0.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#resultsfetch_translation_selling" ).load( "<?php echo site_url; ?>/back_end/fetch_translation_selling.php"); //load initial records
	$(".resultsfetch_translation_selling_loading-div").hide(); //show loading element
	$("#resultsfetch_translation_selling").on( "click", ".pagination a", function (e){
		e.preventDefault();
		$(".resultsfetch_translation_selling_loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#resultsfetch_translation_selling").load("<?php echo site_url; ?>/back_end/fetch_translation_selling.php",{"page":page}, function(){ //get content from PHP page
			$(".resultsfetch_translation_selling_loading-div").hide(); //show loading element
		});		
	});

	$("#resultsfetch_transcription_selling" ).load( "<?php echo site_url; ?>/back_end/fetch_transcription_selling.php"); //load initial records
	$(".resultsfetch_transcription_selling_loading-div").hide(); //show loading element
	$("#resultsfetch_transcription_selling").on( "click", ".pagination a", function (e){
		e.preventDefault();
		$(".resultsfetch_transcription_selling_loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#resultsfetch_transcription_selling").load("<?php echo site_url; ?>/back_end/fetch_transcription_selling.php",{"page":page}, function(){ //get content from PHP page
			$(".resultsfetch_transcription_selling_loading-div").hide(); //show loading element
		});		
	});

	$("#resultsfetch_sub_titling_selling" ).load( "<?php echo site_url; ?>/back_end/fetch_sub_titling_selling.php"); //load initial records
	$(".resultsfetch_sub_titling_selling_loading-div").hide(); //show loading element
	$("#resultsfetch_sub_titling_selling").on( "click", ".pagination a", function (e){
		e.preventDefault();
		$(".resultsfetch_sub_titling_selling_loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#resultsfetch_sub_titling_selling").load("<?php echo site_url; ?>/back_end/fetch_sub_titling_selling.php",{"page":page}, function(){ //get content from PHP page
			$(".resultsfetch_sub_titling_selling_loading-div").hide(); //show loading element
		});		
	});

	$("#resultsfetch_proofreading_selling" ).load( "<?php echo site_url; ?>/back_end/fetch_proofreading_selling.php"); //load initial records
	$(".resultsfetch_proofreading_selling_loading-div").hide(); //show loading element
	$("#resultsfetch_proofreading_selling").on( "click", ".pagination a", function (e){
		e.preventDefault();
		$(".resultsfetch_proofreading_selling_loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#resultsfetch_proofreading_selling").load("<?php echo site_url; ?>/back_end/fetch_proofreading_selling.php",{"page":page}, function(){ //get content from PHP page
			$(".resultsfetch_proofreading_selling_loading-div").hide(); //show loading element
		});		
	});

	$("#resultsfetch_typing_selling" ).load( "<?php echo site_url; ?>/back_end/fetch_typing_selling.php"); //load initial records
	$(".resultsfetch_typing_selling_loading-div").hide(); //show loading element
	$("#resultsfetch_typing_selling").on( "click", ".pagination a", function (e){
		e.preventDefault();
		$(".resultsfetch_typing_selling_loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#resultsfetch_typing_selling").load("<?php echo site_url; ?>/back_end/fetch_typing_selling.php",{"page":page}, function(){ //get content from PHP page
			$(".resultsfetch_typing_selling_loading-div").hide(); //show loading element
		});		
	});

});
</script>

<?php

//$_SESSION['message'] = "A BNGH ";
$importTableListArray = array(
         'tableGroup'=> array(
		     'translation'=>array(
			    'selling'=>'selling',
				'domain'=>'domain',
				'quality'=>'quality',
				'penalty'=>'penalty'),
			'transcription'=>array(
			    'selling'=>'selling',
				'domain'=>'domain',
				'quality'=>'quality',
				'penalty'=>'penalty'),
			'sub_titling'=>array(
			    'selling'=>'selling',
				'domain'=>'domain',
				'quality'=>'quality',
				'penalty'=>'penalty'),
			'proofreading'=>array(
			    'selling'=>'selling',
				'domain'=>'domain',
				'quality'=>'quality',
				'penalty'=>'penalty'),
			'typing'=>array(
			    'selling'=>'selling',
				'domain'=>'domain',
				'quality'=>'quality',
				'penalty'=>'penalty'),		 
		 )		 
);


$impFileHeaderArray = array(

		     'selling'=>array('Source_Language','Target_Language',' Price_Per_Word','Price_Per_Page','Minimum_Bill','Shan_Certification_Cost','Words_In_A_Day','Pages_In_A_day','Strength'),
			 'domain'=>array('Domain_Name','Add_Percent_To_Cost','Add_Days'),
			 'quality'=>array('Pro_Efficiency_Level','Add_Percent_To_Cost','Add_Days'),
			 'penalty'=>array('Strength','Penalty_Percent')
);


if( isset($_REQUEST['serviceFlag'])  && ( $_REQUEST['serviceFlag'] == 'yes') ){

//print_r($importTableListArray);
	$tableGroup  = $_REQUEST['tableGroup'];
	$serviceType = $_REQUEST['serviceType'];
   
    $tableGroupSTR = '';
	$serviceTypeSTR = '';
   foreach($importTableListArray as $key=>$val)
   {  
      
		 if(is_array($val))
		 {
            foreach($val AS $key1=>$val1)
			{

					  if( $key1 == $tableGroup){
						  $tableGroupSTR = $key1;
						  if(is_array($val))
						  {
							  foreach($val1 as $key2=>$val2)
							  {
								 if( $key2 == $serviceType){
									$serviceTypeSTR = $key2;
								 }
							  }
						  }
					  }
			}
		 }
   }

        $import_table_name = 'tbl_'.$tableGroupSTR.'_'.$serviceTypeSTR;
   
   
		$handle = fopen($_FILES['imp_file']['tmp_name'], "r");		
		$i=0;
		$count=0;
		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
		{   $i++;
			
			if( $i == 1)
			{	
				$sizeFileHeaderData = sizeof($data);
				$sizeValidHeaderData = sizeof($impFileHeaderArray[$serviceTypeSTR]);    
				
				
				if($sizeFileHeaderData == $sizeValidHeaderData )
				{  
				   $fileHdFlag = 1;
				   foreach($data as $key => $val ){
					 $comFHVal = strtolower($val);
					 $comVHVal = strtolower( $impFileHeaderArray[$serviceTypeSTR][$key] );  
					 if( $comVHVal != $comVHVal){
						$fileHdFlag = 2;
						$message = " There is invalid file";
						$_SESSION['message'] = $message;
						$siteHead = site_url.'/index2.php';
						header("Location: ".$siteHead);
						exit;
					 }
				   }
				     
					  $sqlTrucate = '';
					  if( $fileHdFlag == 1) {
						  $sqlTrucate = "TRUNCATE TABLE {$import_table_name} ";
						  $query = mysql_query($sqlTrucate);
						  
					  }					  				   				  					

				}else{					
					$message =  "Invalid file";
					$_SESSION['message'] = $message;
					$siteHead = site_url.'/index2.php';
					header("Location: ".$siteHead);
					exit;
				}
				
				
		   }else
		   {
					if( $serviceTypeSTR == 'domain')
					{
						
						$name = $data[0];
						$add_percent_2 = $data[1];
						$add_days_2 = $data[2];                        
						$sql = "INSERT INTO $import_table_name SET name = '$name', add_percent_2 = '$add_percent_2', add_days_2 = '$add_days_2', created_on = NOW(), status = '1' ";
						$query = mysql_query($sql);
						//$num_rows = mysql_affect_rows($query);
						$count = $count + 1;

					}elseif($serviceTypeSTR == 'penalty'){
						$strength = $data[0];
						$penalty_percent = $data[1];						                        
						$sql = "INSERT INTO $import_table_name SET strength = '$strength', penalty_percent = '$penalty_percent', created_on = NOW(), status = '1' ";
						$query = mysql_query($sql);
						//$num_rows = mysql_affect_rows($query);
						$count = $count + 1;

					}elseif($serviceTypeSTR == 'quality'){
						$pro_level = $data[0];
						$add_percent_2 = $data[1];
						$add_days_2 = $data[2];
						$sql = "INSERT INTO $import_table_name SET pro_level = '$pro_level', add_percent_2 = '$add_percent_2', add_days_2 = '$add_days_2', created_on = NOW(), status = '1' ";
						$query = mysql_query($sql);
						//$num_rows = mysql_affect_rows($query);
						$count = $count + 1;
					}elseif($serviceTypeSTR == 'selling'){
						$source_language = $data[0];
						$target_language = $data[1];
						$price_per_word = $data[2];
						$price_per_page = $data[3];
						$minimum_bill = $data[4];
						$shan_certification = $data[5];
						$words_in_a_day = $data[6];
						$pages_in_a_day = $data[7];
						$strength = $data[8];

						$sql = "INSERT INTO $import_table_name SET source_language = '$source_language', target_language = '$target_language', price_per_word = '$price_per_word', price_per_page = '$price_per_page', minimum_bill = '$minimum_bill', shan_certification = '$shan_certification', words_in_a_day = '$words_in_a_day', pages_in_a_day = '$pages_in_a_day', strength = '$strength', created_on = NOW(), status = '1' ";
						$query = mysql_query($sql);
						//$num_rows = mysql_affect_rows($query);
						$count = $count + 1;

					}		        
               
		   }

		  
		}

	
		$message = "There are total {$count} inserted successfully. ";
		$_SESSION['message'] = $message;
		$siteHead = site_url.'/index2.php';
		header("Location: ".$siteHead);
		exit;
	

}




?>

</head>

<body>
<?php  require_once(ABSPATH.'front_end/shan_trans_header.php'); ?>
    <section>
    	<div class="container">
        	<div class="row">
        	<div class="mid-sec">
			<?php 
			  if(isset($_SESSION['message'])){ ?>
			   <div class="service-hed" style="border:1px solid #CCCCCC;" >
					<?php 
					print_r($_SESSION['message']) ;
					unset($_SESSION['message']);
					?>
				</div>

		   <?php  } ?>			
                <div class="service-hed">Service Management</div>
                    <div class="mid-sec1">
                    <div class="ser-bg">
                            <div class="tabs-7">
								<ul class="tabs">
									<li><a href="#tab25" onClick="return tbShowHide('translation_toggle_dv');" ><i class="fa fa-check" aria-hidden="true"></i> Translation</a></li>
									<li><a href="#tab26" onClick="return tbShowHide('transcription_toggle_dv');"  ><i class="fa fa-random" aria-hidden="true"></i> Transcription</a></li>
									<li><a href="#tab27" onClick="return tbShowHide('sub_titling_toggle_dv');"  ><i class="fa fa-random" aria-hidden="true"></i> Sub Titling</a></li>
									<li><a href="#tab28" onClick="return tbShowHide('proofreading_toggle_dv');"  ><i class="fa fa-random" aria-hidden="true"></i> Proofreading</a></li>
									<li><a href="#tab29" onClick="return tbShowHide('typing_toggle_dv');"  ><i class="fa fa-random" aria-hidden="true"></i> Typing</a></li>
									<li><a href="#tab30" ><i class="fa fa-random" aria-hidden="true"></i> Service Setting</a></li>
								</ul>
								<section class="tab_content_wrapper">
									<?php require_once(ABSPATH.'back_end/service_tab_upload.php'); ?>
								</section>
							</div>
                </div>
                <div class="clr"></div>
                <hr />

									
            </div>
				<div class="clr">&nbsp;</div>
				<div class="clr">&nbsp;</div>					 			    
				<?php require_once(ABSPATH.'back_end/translation_toggle.php'); ?>						
				<?php require_once(ABSPATH.'back_end/transcription_toggle.php'); ?>
				<?php require_once(ABSPATH.'back_end/sub_titling_toggle.php'); ?>
				<?php require_once(ABSPATH.'back_end/proofreading_toggle.php'); ?>
				<?php require_once(ABSPATH.'back_end/typing_toggle.php'); ?>						
            </div>
        </div>
    </section>
    
    <footer class="foot-sec">
      <div class="container">
      	<div class="row"></div>
      </div>
    </footer>
    
</body>

<!-----for tab----->
<link href="css/jQueryTab.css" rel="stylesheet" type="text/css">
<link href="css/animation.css" rel="stylesheet" type="text/css">


<script src="js/jQueryTab.js"></script> 
<script type="text/javascript">
// initializing jQueryTab plugin 

$('.tabs-7').jQueryTab({
    initialTab: 2,
    tabInTransition: 'fadeIn',
    tabOutTransition: 'fadeOut',
    cookieName: 'active-tab-7'
    
});

function tbShowHide(id){
	var divID = id;
		var obj = {"translation_toggle_dv": "translation_toggle_dv",
		  "sub_titling_toggle_dv": "sub_titling_toggle_dv",
		  "transcription_toggle_dv": "transcription_toggle_dv",
		  "proofreading_toggle_dv": "proofreading_toggle_dv",
		  "typing_toggle_dv": "typing_toggle_dv",
		};
		$.each( obj, function( key, value ) {
			$('#'+value).hide();
		});	
		$('#'+divID).show();
	
	
	
}

</script>

<!-----for tab2----->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/master.js"></script>

</html>
