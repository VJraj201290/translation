<?php include("connection.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<?php	

	if(!isset($_SESSION['SESS_id']))
	{
		$siteHead = site_url.'/index.php';
		header("Location: ".$siteHead);
		exit;

		
	}

  $user_id = $_SESSION['SESS_id'];


  $tabId = 1;
  if( isset( $_REQUEST['tabId']))
  {
    $tabId = $_REQUEST['tabId'];
  }

  $pendingActive = ( $tabId == 1 ) ? 'in active' : '';
  $progressActive = ( $tabId == 2 ) ? 'in active' : '';
  $completeActive = ( $tabId == 3 ) ? 'in active' : '';
  $transactionActive = ( $tabId == 4 ) ? 'in active' : '';
  $changeActive = ( $tabId == 5 ) ? 'in active' : '';
  $searchActive = ( $tabId == 6 ) ? 'in active' : '';


  $clPendingActive = ( $tabId == 1 ) ? 'active' : '';
  $clProgressActive = ( $tabId == 2 ) ? 'active' : '';
  $clCompleteActive = ( $tabId == 3 ) ? 'active' : '';
  $clTransactionActive = ( $tabId == 4 ) ? 'active' : '';
  $clChangeActive = ( $tabId == 5 ) ? 'active' : '';
  $clSearchActive = ( $tabId == 6 ) ? 'active' : '';


					
if( isset( $_REQUEST['subdelPendOrder']) ){					   
	if( $_REQUEST['subdelPendOrder'] == 'yes' ){
	   $del_order_id = $_REQUEST['delPendOrder'];
	   $sqlDelUp = "UPDATE tbl_order SET order_status_id = 4 WHERE order_id = '$del_order_id' ";
	   $queryDel = mysql_query($sqlDelUp);
	   $delAffRow = mysql_affected_rows(); 
	   if( $delAffRow > 0){
			$message = "<div class='success' >Delete successfully.</div>";
			$_SESSION['message'] = $message;						   
	   }						   
	}					   
}

	
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/bootstrap.min1.css" rel="stylesheet" type="text/css" />
<link href="css/left-menu.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<title>Shantranslation</title>
</head>

<body>
<?php  require_once(ABSPATH.'front_end/shan_trans_header.php'); ?>
<section>
  <div class="container-fluid">
    <div class="row">
      <div class="mid-sec"> 
        
        <!-- dashboard start-->
        <div class="dashboard-main">
          <div class="dash1">
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 pull-right">
              <!--<button class="dash-btn">Translation Management</button>-->
              <button class="dash-btn actdash">Dashboard</button>
			  <a href="<?php echo site_url; ?>/logout.php" ><button class="dash-btn">Logout</button></a>
           <a href="<?php echo site_url; ?>/" ><button class="dash-btn">Translation Management</button></a>
            </div>
          </div>
          <div class="dash2">
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 pull-right">
              <div class="dash-text">Dashboard</div> <div class="dash-text" style="float:right;margin-right:20%;" >Welcome! <span class="user_dash" >&nbsp;&nbsp;&nbsp;<?php echo $_SESSION['SESS_name']; ?></span></div>
            </div>
          </div>
        </div>
        <!-- dashboard end--> 
        
 
        <div class="col-md-10 pull-right" style="padding-bottom:120px;" > 
		 <?php if(isset($_SESSION['message'])){ echo $_SESSION['message']; echo "<br/>"; unset($_SESSION['message']);  } ?>
          <!--<div class="service-hed">Service Management</div>-->
          <div class="mid-sec1">
            <div class="panel with-nav-tabs1 panel-danger1">
              <div class="panel-heading">
                <ul class="nav1 nav-tabs1">
                  <li class="<?php echo $clPendingActive; ?>"><a href="#tab1danger" data-toggle="tab">Pending Order</a></li>
                  <li class="<?php echo $clProgressActive; ?>"><a href="#tab2danger" data-toggle="tab">Order in Progress</a></li>
                  <li class="<?php echo $clCompleteActive; ?>"><a href="#tab3danger" data-toggle="tab">Completed Order</a></li>
                  <li class="<?php echo $clTransactionActive; ?>"><a href="#tab4danger" data-toggle="tab">Transaction History </a></li>
                  <li class="<?php echo $clChangeActive; ?>"><a href="#tab5danger" data-toggle="tab">Change Password </a></li>
                  <li class="<?php echo $clSearchActive; ?>"><a href="#tab6danger" data-toggle="tab">Search Order </a></li>
                </ul>
              </div>
              <div class="panel-body">
                <div class="tab-content" > 
                  
                  <!--Pending Order start-->
					<?php  require_once(ABSPATH.'user_record/pending_order.php'); ?>
                  <!--Pending Order end--> 
                  
                  <!--Order in Progress start-->
					<?php  require_once(ABSPATH.'user_record/order_in_progress.php'); ?>
                  <!--Order in Progress end--> 
                  
                  <!--Completed Order start-->                  
                  <?php  require_once(ABSPATH.'user_record/complete_order.php'); ?>
                  <!--Completed Order end-->
                  
                   <!--Transaction History start-->
				    <?php  require_once(ABSPATH.'user_record/tranaction_history.php'); ?>
				   <!--Transaction History end--> 
                  
                  <!--Change Password start-->
					<?php  require_once(ABSPATH.'user_record/change_password.php'); ?>
                   <!--Change Password end-->
                   
                   <!--Search Order start-->
					<?php  require_once(ABSPATH.'user_record/search_order.php'); ?>
                   <!--Search Order end-->

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php  require_once(ABSPATH.'front_end/shan_trans_footer.php'); ?>
<input type="hidden" name="tabId" id="tabId" value="<?php echo $tabId; ?>" >
</body>

<!-----for tab2----->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<link href="css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.12.4.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>


<script type="text/javascript" >
		   


			$(document).ready(function(){
				
				 $("#chanPass").click(function(){	
					 
						var errFlag = 0;
						var oldPassword = $("#oldPassword").val();
						var newPassword1 = $("#newPassword1").val();
						var newPassword2 = $("#newPassword2").val();
						var mess = 'Enter following \n \n';
						if(oldPassword == ''){								
							mess = mess + 'Old password. \n';
							errFlag = 1;	
						}

						if(newPassword1 == ''){								
							mess = mess + 'New password. \n';	
							errFlag = 1;	
						}
					
						if(newPassword2 == ''){								
							mess = mess + 'Confirm password. \n';	
							errFlag = 1;	
						}

						if( (newPassword1 != '') && ( newPassword2 != '' )){
						   if( newPassword1 != newPassword2 ){
							mess = mess + 'Password and reconfirm password. \n';	
							errFlag = 1;						   
						   }
						}

						if( errFlag == 1 ){
						  alert(mess);
						  return false;
						}

					var sub_url = '<?php echo site_url; ?>';
					var urlSel = sub_url + "/user_record/ajax_change_password.php";														    

					$.ajax({
						url: urlSel, 
						type : "POST", 
						dataType : 'json', 
						data : $("#changePassfrm").serialize(), 
						success : function(jsonStr) {
							var message = 	jsonStr.message;	   
							$("#changePasswordMess").html(message);				 													  
						},
						error: function(xhr, resp, text) {
							console.log(xhr, resp, text);
						}
					})
					return false;
                   
				});
			});

</script>

</html>
