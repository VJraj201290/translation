<?php include("connection.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<?php	

	if(!isset($_SESSION['SESS_id']))
	{
		$siteHead = site_url.'/index.php';
		header("Location: ".$siteHead);
		exit;

		
	}

  $user_id = $_SESSION['SESS_id'];


  $tabId = 1;
  if( isset( $_REQUEST['tabId']))
  {
    $tabId = $_REQUEST['tabId'];
  }

  $pendingActive = ( $tabId == 1 ) ? 'in active' : '';
  $progressActive = ( $tabId == 2 ) ? 'in active' : '';
  $completeActive = ( $tabId == 3 ) ? 'in active' : '';
  $transactionActive = ( $tabId == 4 ) ? 'in active' : '';
  $changeActive = ( $tabId == 5 ) ? 'in active' : '';
  $searchActive = ( $tabId == 6 ) ? 'in active' : '';


  $clPendingActive = ( $tabId == 1 ) ? 'active' : '';
  $clProgressActive = ( $tabId == 2 ) ? 'active' : '';
  $clCompleteActive = ( $tabId == 3 ) ? 'active' : '';
  $clTransactionActive = ( $tabId == 4 ) ? 'active' : '';
  $clChangeActive = ( $tabId == 5 ) ? 'active' : '';
  $clSearchActive = ( $tabId == 6 ) ? 'active' : '';


if( isset( $_REQUEST['subdelPendOrder']) ){					   
	if( $_REQUEST['subdelPendOrder'] == 'yes' ){
	   $del_order_id = $_REQUEST['delPendOrder'];
	   $sqlDelUp = "UPDATE tbl_order SET order_status_id = 8 WHERE order_id = '$del_order_id' ";
	   $queryDel = mysql_query($sqlDelUp);
	   $delAffRow = mysql_affected_rows(); 
	   if( $delAffRow > 0){
			$message = "<div class='success' >Deleted successfully.</div>";
			$_SESSION['message'] = $message;						   
	   }						   
	}					   
}

if(isset($_POST['update']))
{
	date_default_timezone_set('Asia/Kolkata');
	$date=date('Y-m-d H:i:s');
	echo $q=mysql_query("update tbl_order set order_time_id = '".$_POST['order_time_id']."',currency_code='".$_POST['currency_code']."',source_lang_name='".$_POST['source_lang_name']."',target_lang_name='".$_POST['target_lang_name']."',quantity='".$_POST['quantity']."',netTotal='".$_POST['netTotal']."',discount='".$_POST['discount']."',discount_type='".$_POST['discount_type']."',date_modified='".$date."' where order_id ='".$_POST['order_id']."'");
	
	if($q)
	{
		echo "<script>alert('inserted')</script>";
	}
	else 
	{
	 echo mysql_error();
		die();
	}
}
if( isset( $_REQUEST['actionComplete']) ){					   
	if( $_REQUEST['actionComplete'] == 'yes' ){
	   $proOrder_id = $_REQUEST['progressOrderIDD'];
	   $sqlOrdUp = "UPDATE tbl_order SET order_status_id = 2 WHERE order_id = '$proOrder_id' ";
	  
	   $queryDel = mysql_query($sqlOrdUp);
	   $affAffRow = mysql_affected_rows(); 
	   if( $affAffRow > 0){
			$message = "<div class='success' >Updated successfully.</div>";
			$_SESSION['message'] = $message;						   
	   }						   
	}					   
}
	
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/bootstrap.min1.css" rel="stylesheet" type="text/css" />
<link href="css/left-menu.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<title>Shantranslation</title>
</head>

<body>
<?php  require_once(ABSPATH.'front_end/shan_trans_header.php'); ?>
<section>
  <div class="container-fluid">
    <div class="row">
      <div class="mid-sec"> 
        
		<div class="col-md-2 col-md-2">
		<!-- Start to nav menu -->
		<?php  require_once(ABSPATH.'menu_bar.php'); ?>
		<!-- End to nav menu -->
		</div>       
         
        <div class="col-md-10 pull-right" style="padding-bottom:120px;" > 
			<div class="service-hed" style="margin-top:0px !important;">User order  </div><br/>
			 <?php if(isset($_SESSION['message'])){ echo $_SESSION['message']; echo "<br/>"; unset($_SESSION['message']);  } ?>
          <!--<div class="service-hed">Service Management</div>-->
          <div class="mid-sec1">
            <div class="panel with-nav-tabs1 panel-danger1">
              <div class="panel-heading">
                <ul class="nav1 nav-tabs1">
                  <li class="<?php echo $clPendingActive; ?>"><a href="#tab1danger" data-toggle="tab">Pending Order</a></li>
                  <li class="<?php echo $clProgressActive; ?>"><a href="#tab2danger" data-toggle="tab">Order in Progress</a></li>
                  <li class="<?php echo $clCompleteActive; ?>"><a href="#tab3danger" data-toggle="tab">Completed Order</a></li>
                  <li class="<?php echo $clTransactionActive; ?>"><a href="#tab4danger" data-toggle="tab">Transaction History </a></li>
                  <!-- -<li class="<?php echo $clChangeActive; ?>"><a href="#tab5danger" data-toggle="tab">Change Password </a></li> -->
                  <li class="<?php echo $clSearchActive; ?>"><a href="#tab6danger" data-toggle="tab">Search Order </a></li>
                </ul>
              </div>
              <div class="panel-body">
                <div class="tab-content" > 
				<?php  require_once(ABSPATH.'admuser_record/pending_order.php'); ?>
				<?php  require_once(ABSPATH.'admuser_record/order_in_progress.php'); ?>
				<?php  require_once(ABSPATH.'admuser_record/complete_order.php'); ?>
				<?php  require_once(ABSPATH.'admuser_record/tranaction_history.php'); ?>
				<?php  require_once(ABSPATH.'admuser_record/search_order.php'); ?>                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php  require_once(ABSPATH.'front_end/shan_trans_footer.php'); ?>
<input type="hidden" name="tabId" id="tabId" value="<?php echo $tabId; ?>" >
</body>

<!-----for tab2----->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<link href="css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.12.4.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>

  <script type="text/javascript">
  function delPendingOrder(){
  
		var r = confirm("Are you sure want to delete.");
		if (r == true) {
		   return true;
		} else {
		  return false;
		}
  }
  

  function compProgressOrder(){
  
		var r = confirm("Are you sure to place as complete order.");
		if (r == true) {
		   return true;
		} else {
		  return false;
		}
  }

  </script>
<script type="text/javascript" >
		   


			$(document).ready(function(){
				
				 $("#chanPass").click(function(){	
					 
						var errFlag = 0;
						var oldPassword = $("#oldPassword").val();
						var newPassword1 = $("#newPassword1").val();
						var newPassword2 = $("#newPassword2").val();
						var mess = 'Enter following \n \n';
						if(oldPassword == ''){								
							mess = mess + 'Old password. \n';
							errFlag = 1;	
						}

						if(newPassword1 == ''){								
							mess = mess + 'New password. \n';	
							errFlag = 1;	
						}
					
						if(newPassword2 == ''){								
							mess = mess + 'Confirm password. \n';	
							errFlag = 1;	
						}

						if( (newPassword1 != '') && ( newPassword2 != '' )){
						   if( newPassword1 != newPassword2 ){
							mess = mess + 'Password and reconfirm password. \n';	
							errFlag = 1;						   
						   }
						}

						if( errFlag == 1 ){
						  alert(mess);
						  return false;
						}

					var sub_url = '<?php echo site_url; ?>';
					var urlSel = sub_url + "/user_record/ajax_change_password.php";														    

					$.ajax({
						url: urlSel, 
						type : "POST", 
						dataType : 'json', 
						data : $("#changePassfrm").serialize(), 
						success : function(jsonStr) {
							var message = 	jsonStr.message;	   
							$("#changePasswordMess").html(message);				 													  
						},
						error: function(xhr, resp, text) {
							console.log(xhr, resp, text);
						}
					})
					return false;
                   
				});
			});

</script>

</html>
