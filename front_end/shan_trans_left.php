<div class="col-md-6">
                  <div class="row">
                     <form id="order_summery" enctype="multipart/form-data">
                        <div class="form-group ">
                           <div class="col-md-8 col-xs-12 ">
                              <label for="exampleInputPassword1" class="form-class">Lets Select the currency</label>
                           </div>
                           <div class="col-md-8 col-xs-8 ">
                            <!--  <select class="form-control drp_input" required="" name="currency_id" id="currency_id" onchange="chngCurrency();"> -->
							  <select class="form-control drp_input" required name="currency_id" id="currency_id" onchange="chngCurrency();" >
							    <option value=""> --Select-- </option>
                                <option value="USD"> United States dollar – USD </option>
								<option value="AUD"> Australian dollar - AUD </option>
								<option value="BRL"> Brazilian real - BRL </option>
								<option value="CAD"> Canadian dollar - CAD </option>
								<option value="CZK"> Czech koruna - CZK </option>
								<option value="DKK"> Danish krone - DKK </option>
								<option value="EUR"> Euro - EUR </option>
								<option value="HKD"> Hong Kong dollar - HKD </option>
								<option value="HUF"> Hungarian forint - HUF </option>
								<option value="ILS"> Israeli new shekel - ILS </option>
								<option value="INR"> Indian Rupee - INR </option>
								<option value="JPY"> Japanese yen - JPY </option>
								<option value="MYR"> Malaysian ringgit - MYR </option>
								<option value="MXN"> Mexican peso - MXN </option>
								<option value="TWD"> New Taiwan dollar - TWD </option>
								<option value="NZD"> New Zealand dollar - NZD </option>
								<option value="NOK"> Norwegian krone - NOK </option>
								<option value="PHP"> Philippine peso - PHP </option>
								<option value="PLN"> Polish złoty - PLN </option>
								<option value="GBP"> Pound sterling - GBP </option>
								<option value="RUB"> Russian ruble - RUB </option>
								<option value="SGD"> Singapore dollar - SGD </option>
								<option value="SEK"> Swedish krona - SEK </option>
								<option value="CHF"> Swiss franc - CHF </option>
								<option value="THB"> Thai baht - THB </option>
                              </select>
							  <div id="currency_id_loader" ></div>
							   
                           </div>
                           <div class="col-md-4 col-xs-4 info-circle">
								<a class="" href="#" data-toggle="tooltip" data-placement="right" title="Please select your local currency in which you would like to check the Prices."><i class="fa fa-question question_custom" aria-hidden="true"></i></a>
                           </div>
                        </div>
                        <br>
                        <div class="form-group ">
                           <div class="col-md-8  col-xs-12 div_class">
                              <label for="exampleInputPassword1" class="form-class">What service do you need</label>
                           </div>
                           <div class="col-md-8 col-xs-9">
						     <?php 
							 
							 $query = mysql_query("select * from tbl_services where status = '1' ORDER BY order_wise, name ASC  ");

							 ?>
                             <!-- <select class="form-control" required="" name="service_id" id="service_id" onChange="chngServiceSAjax();"> -->
							  <select class="form-control" required name="service_id" id="service_id" onChange="chngServiceJSAjax();">
								<option value=""  >	--Select-- </option>
								<?php
								while($result=mysql_fetch_array($query))
								{  ?>
								<option value="<?php echo $result['id']; ?>"  >	<?php echo $result['name'];  ?> </option>
								<?php } ?>
                              </select>
                           </div>
                           <div class="col-md-4 col-xs-3 info-circle">
                              <a class="" href="#" data-toggle="tooltip" data-placement="right" title="" data-original-title="Please select the correct option for your project type."><i class="fa fa-question question_custom" aria-hidden="true"></i></a>
                           </div>
                        </div>
                        <div class="form-group ">
                           <div class="col-md-8  col-xs-12 div_class">
                              <label for="exampleInputPassword1" class="form-class">What is your source language</label>
                           </div>
                           <div class="col-md-8 col-xs-9">
                              <select class="form-control drp_input" required name="source_lang_id" id="source_lang_id" onchange="chngSourceLangJSAjax(this);"></select>
							  <div id="source_lang_loader" ></div>
                           </div>
                           <div class="col-md-4 col-xs-3 info-circle">
                              <a class="" href="#" data-toggle="tooltip" data-placement="right" title="" data-original-title="Kindly select the language of the file you wish translated. "><i class="fa fa-question question_custom" aria-hidden="true"></i></a>
                           </div>
                        </div>
                        <div class="form-group ">
                           <div class="col-md-8  col-xs-12 div_class">
                              <label for="exampleInputPassword1" class="form-class">What is your target language</label>
                           </div>
                           <div class="col-md-8 col-xs-9">
                              <select class="form-control drp_input" required multiple="" name="target_lang_id" id="target_lang_id" onchange="chngTargetLangJSAjax();"></select>
							  <div id="target_lang_loader" ></div>
							  <span style="font-size:13px;color:#0092cd;">To select more than one target language press cntrl and select. </span>
                           </div>
                           <div class="col-md-4 col-xs-3 info-circle">
                              <a class="" href="#" data-toggle="tooltip" data-placement="right" title="" data-original-title="You are requesting for file conversion in this language. You may select more than one target language."><i class="fa fa-question question_custom" aria-hidden="true"></i></a>
                           </div>
                        </div>
                       
                        <div class="form-group ">
                           <div class="col-md-8 col-xs-12 div_class">
                              <label for="exampleInputPassword1" class="form-class">Expertise</label>
                           </div>
                           <div class="col-md-8 col-xs-9">
                              <select class="form-control drp_input" name="domain_id"  id="domain_id"  onchange="chngDomainJSAjax();" ></select>
							  <div id="domain_loader" ></div>
                           </div>
                           <div class="col-md-4 col-xs-3 info-circle">
                              <a class="" href="#" data-toggle="tooltip" data-placement="right" title="" data-original-title="Kindly specify the Domain of your project, to locate the best translator for you."><i class="fa fa-question question_custom" aria-hidden="true"></i></a>
                           </div>
                        </div>
                        <div class="form-group ">
                           <div class="col-md-8 col-xs-12 div_class">
                              <label for="exampleInputPassword1" class="form-class">Quality</label>
                           </div>
                           <div class="col-md-8 col-xs-9 ">
                              <select class="form-control drp_input" name="quality_id" id="quality_id" onchange="chngQualityJSAjax();" ></select>
							  <div id="quality_loader" ></div>
                           </div>
                           <div class="col-md-4 col-xs-3 info-circle">
                              <a class="" href="#" data-toggle="tooltip" data-placement="right" title="" data-original-title="Depending upon your required precision kindly select here."><i class="fa fa-question question_custom" aria-hidden="true"></i></a>
                           </div>
                        </div>
                        <div class="col-md-8 col-xs-12 div_class">
                           <label for="exampleInputPassword1" class="form-class">Is certifiction necessary</label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-8 col-xs-12 div_class">
                           <div class="radio">
								<label class="radio-inline"><input name="is_certification" id="is_1" value="1" onchange="chngCertification();" type="radio">Yes</label>
								<label class="radio-inline"><input name="is_certification" id="is_0" value="0" checked="" onchange="chngCertification();" type="radio">No</label>
							</div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group ">
                           <div class="col-md-8 col-xs-12 div_class">
                              <label for="exampleInputEmail1" class="form-class" id="label_noofwords">Enter the number of <span id="wordMinutus" >words</span></label>
                           </div>
                           <div class="col-md-8 col-xs-9 ">
                              <div class="form-group">
                                 <input class="form-control" id="no_of_words" name="no_of_words" placeholder="" onkeyup="value=isNaN(value)?0:value;chng_noofpages();" autocomplete="off" type="number" value="" >
                                 <label for="exampleInputEmail1" class="orage-icon" style="color:#0092cd">or</label>
                              </div>
                           </div>
                           <div class="col-md-4 col-xs-3 info-circle">
                              <a class="" href="#" data-toggle="tooltip" data-placement="right" title="" data-original-title="Please mention the no. of words/ minutes in your source document/ Audio- Video"><i class="fa fa-question question_custom" aria-hidden="true"></i></a>
                           </div>
                        </div>
                        <div class="form-group ">
                           <div class="col-md-8 col-xs-12">
                              <label for="exampleInputEmail1" class="form-class" id="label_noofpages">Enter the number of <span id="pageHours" >pages</label>
                           </div>
                           <div class="col-md-8 col-xs-9 ">
                              <div class="form-group">
                                 <input class="form-control" id="no_of_pages" name="no_of_pages" placeholder="" onkeyup="value=isNaN(value)?0:value;chng_noofpages();" autocomplete="off" type="number" value="">
								  <label for="exampleInputEmail1" class="orage-icon" style="color:#0092cd">or</label>
                              </div>
                           </div>
                           <div class="col-md-4  col-xs-3 info-circle">
                              <a class="" href="#" data-toggle="tooltip" data-placement="right" title="" data-original-title="Please mention the no. of pages/ hours in your source document/ Audio- Video. A standard page has around 300 words."><i class="fa fa-question question_custom" aria-hidden="true"></i></a>
                           </div>
                        </div>
						
						<!--<input type="button" id="sub_files" onClick="submit_files();" name="sub_files" value="Calculate word" />-->
                        <div class="col-md-10 " style="display:none;">
                           <ul class="file-type_indicate">
                              <li>Supported file types</li>
                              
                           </ul>
                        </div>
						
                     </form>
                  </div>

				  <div id="domainSql" style="display:none;" ></div><BR/>
				  <div id="qualitySql" style="display:none;"  ></div><BR/>
				  <div id="sellingSql" style="display:none;"  ></div><BR/>
				  <div id="penalitySQL" style="display:none;"  ></div><BR/>
				  <div id="currency_code" style="display:none;" ></div><BR/>
</div>


<script type="text/javascript" >

   function chngCurrency(){
	   
		var sub_url = '<?php echo site_url; ?>';	
	   
	   //alert(sub_url);
		var currency_id = $("#currency_id").val();
		var currency_job_input_txt = $('#currency_job_input_txt').html(currency_id);
	   //alert(currency_job_input_txt);
		var currency_job_input_name = $('#currency_job_input_name').val(currency_id);
		var currency_job_input = $('#currency_job_input').val(currency_id);
		$('#totlaCurrencyCalculating').html(currency_id);
		$('#select_currency_id').html(currency_id);
		

		$("#curr_curency_value").val('');
		var imgLoad = '<img src="'+sub_url+'/images/LoadingData.gif">'
		$("#currency_id_loader").html(imgLoad);
		if( currency_id == ""){
		   return false;
		}
		
		var urlSel = sub_url + "/front_end/currency_converter.php";
		$.ajax({
			url: urlSel,
			type: "POST",
			data: {
			   currency_id: currency_id
			},
			dataType: "JSON",
			success: function (jsonStr) {	 
				    var currencyValue = jsonStr.currencyValue;
					var to_currency = jsonStr.to_currency;
					$("#curr_curency_value").val(currencyValue);
					$("#spanCalculating").html("Calculating...");
					$("#currency_id_loader").html('');
					calculate('chngCertification');  
			}
		});
	

   
   }

  function chngCertification(){
    
		calculate('chngCertification');  
  }
   
   function chngServiceSAjax(){ 
						
						var sub_url = '<?php echo site_url; ?>';	
						var service_id = $("#service_id").val();
						
						var urlSel = sub_url + "/front_end/ajax_source_langugae.php";
						if( service_id == ''){
						  alert("Select services");
						  return false;
						}

						$.ajax({				 
						 type:'POST',
						 url:urlSel,
						 data: "service_id="+service_id,
						 success:function(text){
							if(text){		
								$("#source_lang_id").html(text);						
								$("#source_lang_loader").html(text);	
                                
								calculate('chngServiceSAjax');
							} 
						 },				  				 
						});
						
		}



		function chngServiceJSAjax(){ 
			
						var sub_url = '<?php echo site_url; ?>';	
						var service_id = $("#service_id").val();
						var service_option = $('#service_id').find(":selected").text();

						$('#service_job_input_txt').html(service_option);
						$('#service_job_input_name').val(service_option);
						$('#service_job_input').val(service_id);	

						var imgLoad = '<img src="'+sub_url+'/images/LoadingData.gif">'						
						var urlSel = sub_url + "/front_end/ajax_source_langugae.php";
						if( service_id == ''){
						  alert("Select services");
						  return false;
						}

						var no_of_words = jQuery("#no_of_words").val();
						var no_of_pages = jQuery("#no_of_pages").val();	

						if( (service_id ==  1) || (service_id == 4) || (service_id ==  5)){
							$("#wordMinutus").html('Words');
							$("#pageHours").html('Pages');
														
							if(no_of_words != ""){
								jQuery("#measure_input_name").val('Words');
								jQuery("#measure_input_txt").html("Words");
							}else if(no_of_pages != ""){
								jQuery("#measure_input_name").val('Pages');
								jQuery("#measure_input_txt").html("Pages");
							}
							
							/*

                            */ 
						}else if((service_id ==  2) || (service_id ==  3)){
							
							if(no_of_words != ""){
								jQuery("#measure_input_name").val('Minutes');
								jQuery("#measure_input_txt").html("Minutes");
							}else if(no_of_pages != ""){
								jQuery("#measure_input_name").val('Hours');
								jQuery("#measure_input_txt").html("Hours");
							}							
							
							$("#wordMinutus").html('Minutes');
							$("#pageHours").html('Hours');						
						}else{
							$("#wordMinutus").html('Words');
							$("#pageHours").html('Pages');
						}

						$("#source_lang_loader").html(imgLoad);
						$("#domain_loader").html(imgLoad);
						$("#quality_loader").html(imgLoad);
						
						///////////
						$.ajax({
							url: urlSel,
							type: "POST",
							data: {
								service_id: service_id
							},
							dataType: "JSON",
							success: function (jsonStr) {									   
								var source_language = jsonStr.source_language;				
								var source_message = jsonStr.source_message;	
								
							
								
								
								var domain_list = jsonStr.domain_list;	
								$("#domain_id").html(domain_list);
								var domain_message = jsonStr.domain_message;

								var quality_list = jsonStr.quality_list;
								$("#quality_id").html(quality_list);
								
								var quality_message = jsonStr.quality_message;

								$("#source_lang_id").html(source_language);						
								$("#source_lang_loader").html(source_message);																					
								$("#domain_loader").html(domain_message);
												
								$("#quality_loader").html(quality_message);

								chngSourceLangJSAjax();	
								
								var source_lang_id = $("#source_lang_id").val();
								var source_lang_option = $('#source_lang_id').find(":selected").text();								
								$('#source_lang_job_input_txt').html(source_lang_option);
								$('#source_lang_job_input_name').val(source_lang_option);
								$('#source_lang_job_input').val(source_lang_id);
                               
								var quality_id = $("#quality_id").val();
								var quality_id_option = $('#quality_id').find(":selected").text();								                               
								$("#spanCalculating").html("Calculating...");
                                calculate('chngServiceJSAjax');
							}
						});
						///////////						
		}

	// 	

  
       function chngDomainJSAjax(){
	            $("#spanCalculating").html("Calculating...");
				calculate('chngDomainJSAjax');
	   }

       function chngQualityJSAjax(){
	   	        $("#spanCalculating").html("Calculating...");
				calculate('chngQualityJSAjax');
	   }

		function chngSourceLangJSAjax(){ 			
						var sub_url = '<?php echo site_url; ?>';	
						var service_id = $("#service_id").val();
						var source_lang_id = $("#source_lang_id").val();
						
						var urlSel = sub_url + "/front_end/ajax_target_langugae.php";
						if( service_id == ''){
						  alert("Select services");
						  return false;
						}

						if( source_lang_id == ''){
						  alert("Select source language");
						  return false;
						}
						var source_lang_option = $('#source_lang_id').find(":selected").text();
						//alert(source_lang_id + '-- source_lang_id' ) ;
						//alert(source_lang_option + '-- source_lang_option' ) ;

						///////////
						$.ajax({
							url: urlSel,
							type: "POST",
							data: {
								service_id: service_id,
								source_lang_id : source_lang_id,
								source_lang_option : source_lang_option
							},
							dataType: "JSON",
							success: function (jsonStr) {									   
								var source_language = jsonStr.source_language;
								var target_language = jsonStr.target_language;	 // 
								var sqlSTR = jsonStr.sqlSTR;
								//alert('sqlSTRsss--' + sqlSTR);
								var message = jsonStr.message;																				
								$("#target_lang_id").html(target_language);						
								$("#target_lang_loader").html(message);	


								var source_lang_id = $("#source_lang_id").val();
								var source_lang_option = $('#source_lang_id').find(":selected").text();								
								$('#source_lang_job_input_txt').html(source_lang_option);
								$('#source_lang_job_input_name').val(source_lang_option);
								$('#source_lang_job_input').val(source_lang_id);

                                $("#spanCalculating").html("Calculating...");
								calculate('chngSourceLangJSAjax');

							}
						});
						///////////						
		}



		function chngTargetLangJSAjax(){ // alert('What is your target language');	 	
						var sub_url = '<?php echo site_url; ?>';	
						var service_id = $("#service_id").val();
						var source_lang_id = $("#source_lang_id").val();
						
						var urlSel = sub_url + "/front_end/ajax_target_langugae_cost.php";
						if( service_id == ''){
						  alert("Select services");
						  return false;
						}

						if( source_lang_id == ''){
						  alert("Select source language");
						  return false;
						}

						var domain_id = $("#domain_id").val();
						if( domain_id == ''){
						  alert("Select expertise");
						  return false;
						}
						var quality_id = $("#quality_id").val();
						if( quality_id == ''){
						  alert("Select quality");
						  return false;
						}

						var target_lang_id = $("#target_lang_id").val();
						if( target_lang_id == ''){
						  alert("Select target language");
						  return false;
						}


						var target_lang_nameSTR = '';
						var tcount = 0 ;
						$("#target_lang_id option:selected").each(function () {		tcount = tcount + 1 ;							   
							if( tcount > 1)
							{ 
								target_lang_nameSTR = target_lang_nameSTR +  ' | ' +$(this).text();
							}else{
								target_lang_nameSTR = target_lang_nameSTR + $(this).text(); 
							}
						});
						$("#target_lang_job_input_txt").html(target_lang_nameSTR);


					//	alert( target_lang_id + " ----ss BB");
						
						//var is_certification = $('is_certification').val();
						var is_certification = $('input[name=is_certification]:checked').val();

						var no_of_words = $('#no_of_words').val();
						var no_of_pages = $('#no_of_pages').val();

						if( ( no_of_words == '' ) && ( no_of_pages == ''  ) ){
						  return false;
						}

                       //   alert("Go ing to progress " );


						var imgLoad = '<img src="'+sub_url+'/images/LoadingData.gif">';
						//$("#projectCostProcessing").html(imgLoad);

						/*
						alert('service_id --' + service_id );
						alert('source_lang_id --' + source_lang_id );

						alert('target_lang_id --' + target_lang_id );

						alert('domain_id --' + domain_id );
						*/
						//alert('quality_id --' + quality_id );

						
					//	alert('is_certification --' + is_certification );

					//	alert('no_of_words --' + no_of_words );
					//	alert('no_of_pages --' + no_of_pages );
						

						//alert('urlSel --' + urlSel );


					   // projectCostProcessing
						///////////
						
						$.ajax({
							url: urlSel,
							type: "POST",							
							data: {

								service_id: service_id,
								source_lang_id : source_lang_id,								
								target_lang_id : target_lang_id,
								domain_id : domain_id,
								quality_id : quality_id,
								is_certification : is_certification,
								no_of_words : no_of_words,
								no_of_pages : no_of_pages

							},
							dataType: "JSON",
							success: function (jsonStr) {									   
								
									$("#spanCalculating").html("Calculating...");
									//alert('What is your target language');	

									var domainSql = jsonStr.domainSql;
									//alert('domainSql --' + domainSql);
									var qualitySql = jsonStr.qualitySql;
									//alert('qualitySql --' + qualitySql);
									//var sellingSql = jsonStr.sellingSql;
									//alert('sellingSql --' + sellingSql);																	

									var base_price_message = jsonStr.base_price_message;
									//alert('base_price_message --' + base_price_message);

									var certification_message = jsonStr.certification_message;
									//alert('certification_message --' + certification_message);

									var domain_message = jsonStr.domain_message;
									//alert('domain_message --' + domain_message);

									var quality_message = jsonStr.quality_message;
									//alert('quality_message --' + quality_message);

									var TotalSUM_base_price = jsonStr.TotalSUM_base_price;
									//alert('TotalSUM_base_price --' + TotalSUM_base_price);

									var TotalSUM_certification = jsonStr.TotalSUM_certification;
									//alert('TotalSUM_certification --' + TotalSUM_certification);

									var TotalSUM_domain = jsonStr.TotalSUM_domain;
									//alert('TotalSUM_domain --' + TotalSUM_domain);

									var TotalSUM_quality = jsonStr.TotalSUM_quality;
									//alert('TotalSUM_quality --' + TotalSUM_quality);  
                                   

									var TotalProject_cost = jsonStr.TotalProject_cost;									
									//alert('TotalProject_cost --' + TotalProject_cost);


									var penalityFlag = jsonStr.penalityFlag;									
									var penalitySTR = jsonStr.penalitySTR;																		
									if( penalityFlag == 1)
									{
										$("#penalty_div").show();
										$("#no_penalty_div").hide();
										$("#penalltySTRID").html(penalitySTR);
									}else{
										$("#penalty_div").hide();
										$("#no_penalty_div").show();									
									}									
									//alert('penalitySTR --' + penalitySTR);

									var penalitySTR = jsonStr.penalitySTR;
									$("#penalltySTRID").html(penalitySTR);
									//alert('penalitySTR --' + penalitySTR);
									
									var TotalDaysSpend = jsonStr.TotalDaysSpend;
									//alert('TotalDaysSpend --' + TotalDaysSpend);								
																	      
									//jQuery('#total_span').html('Calculating...');
									//setTimeout(function(){ calculate(); }, 1000);

									calculate('chngTargetLangJSAjax');
									return false;



							}
						});
						
						///////////						
		}




		 function chng_noofpages()
		 {
			var no_of_words = jQuery("#no_of_words").val();
			var no_of_pages = jQuery("#no_of_pages").val();
            var service_id = $("#service_id").val();

			$("#spanCalculating").html("Calculating...");
			if(no_of_words != ""){
				jQuery("#no_of_pages").prop('disabled', true);
				jQuery("#measure_input").val(no_of_words);

				$('#quantity_input_txt').html(no_of_words);
				$('#quantity_input').val(no_of_words);	
				
				if( (service_id ==  1) || (service_id == 4) || (service_id ==  5)){
					jQuery("#measure_input_name").val('Words');
					jQuery("#measure_input_txt").html("Words");
				}else if((service_id ==  2) || (service_id ==  3)){
					jQuery("#measure_input_name").val('Minutes');
					jQuery("#measure_input_txt").html("Minutes");				
				}				
			}
			else if(no_of_pages != ""){
				jQuery("#no_of_words").prop('disabled', true);				
				jQuery("#measure_input").val(no_of_pages);
				$('#quantity_input_txt').html(no_of_pages);
				$('#quantity_input').val(no_of_pages);


				if( (service_id ==  1) || (service_id == 4) || (service_id ==  5)){
					jQuery("#measure_input_name").val('Pages');
					jQuery("#measure_input_txt").html("Pages");
				}else if((service_id ==  2) || (service_id ==  3)){
					jQuery("#measure_input_name").val('Hours');
					jQuery("#measure_input_txt").html("Hours");				
				}

				
			}
			else{
				jQuery("#no_of_pages").prop('disabled', false);
				jQuery("#no_of_words").prop('disabled', false);
				jQuery("#measure_input").val("");
				jQuery("#measure_input_name").val('');
				jQuery("#measure_input_txt").html("");

				$('#quantity_input_txt').html("");
				$('#quantity_input').val("");
				
			}			
			calculate( 'chng_noofpages' );
		 }


            
		 function calculate(fname)
	     {
						if( fname == 'chngCertification'){
							 $("#spanCalculating").html("0.00");
						}else{
							 $("#spanCalculating").html("Calculating...");
						}
						
						$("#domainSql").html("");
						$("#qualitySql").html("");
						$("#sellingSql").html("");
						$("#penalitySQL").html("");

						$("#TotalSUM_base_price_input").val(0);
						$("#TotalSUM_base_price_input").val(0);
						$("#TotalSUM_certification_input").val(0);
						$("#TotalSUM_domain_input").val(0);
						$("#TotalSUM_quality_input").val(0);
						$("#NetTotal_input").val(0);

						//alert('calculatefname-----' + fname);
						
						var sub_url = '<?php echo site_url; ?>';	
						var service_id = $("#service_id").val();
						var source_lang_id = $("#source_lang_id").val();
						
						var urlSel = sub_url + "/front_end/ajax_target_langugae_cost.php";
						
						var currency_id = $("#currency_id").val();
						if( currency_id == "" ){
						   return false;
						}

						// curr_curency_value
                         var curr_curency_value = $("#curr_curency_value").val();
						 
						 if( curr_curency_value == "" ){
							 alert(curr_curency_value);
							 return false;
						 }

						var no_of_words = jQuery("#no_of_words").val();
						var no_of_pages = jQuery("#no_of_pages").val();							
						if( ( no_of_words == "" ) && ( no_of_pages == "" ) ){
						   return false;
						}
						
						if( service_id == ''){
						  //alert("Select services");
						  return false;
						}

						if( source_lang_id == ''){
						  //alert("Select source language");
						  return false;
						}

						var domain_id = $("#domain_id").val();
						if( domain_id == ''){
						  //alert("Select expertise");
						  return false;
						}
						//var quality_id = $("#quality_id").val();
						var quality_id  = document.getElementById("quality_id").value;
						if( quality_id == ''){
						 // alert("Select quality");
						  return false;
						}
						

						var target_lang_id = $("#target_lang_id").val();
						if( target_lang_id == ''){
						  //alert("Select target language");
						  return false;
						}

						var is_certification = $('input[name=is_certification]:checked').val();
						var no_of_words = $('#no_of_words').val();
						var no_of_pages = $('#no_of_pages').val();
						
						if( ( no_of_words == '' ) && ( no_of_pages == ''  ) ){
						  return false;
						}
						var imgLoad = '<img src="'+sub_url+'/images/LoadingData.gif">';

						$.ajax({
							url: urlSel,
							type: "POST",
							data: {

								currency_id : currency_id,
                                curr_curency_value : curr_curency_value,
								service_id : service_id,
								source_lang_id : source_lang_id,								
								target_lang_id : target_lang_id,
								domain_id : domain_id,
								quality_id : quality_id,
								is_certification : is_certification,
								no_of_words : no_of_words,
								no_of_pages : no_of_pages

							},
							dataType: "JSON",
							success: function (jsonStr) {									   
								
									//alert("Calculate NKL ");							
									var domainSql = jsonStr.domainSql;
									// alert('domainSql --' + domainSql);
									var qualitySql = jsonStr.qualitySql;
									// alert('qualitySql --' + qualitySql);
									var sellingSql = jsonStr.sellingSql;
									// alert('sellingSql --' + sellingSql); currency_code 
									var penalitySQL = jsonStr.penalitySQL;
									// alert('penalitySQL --' + penalitySQL);
									var currency_code = jsonStr.currency_code;
									//alert('currency_code--' + currency_code);
									
									$("#domainSql").html(domainSql);
									$("#qualitySql").html(qualitySql);
									$("#sellingSql").html(sellingSql);
									$("#penalitySQL").html(penalitySQL);									
									$("#currency_code").html(currency_code);	


									var base_price_message = jsonStr.base_price_message;
									// alert('base_price_message --' + base_price_message);

									var certification_message = jsonStr.certification_message;
									// alert('certification_message --' + certification_message);
									

									var domain_message = jsonStr.domain_message;									
									// alert('domain_message --' + domain_message);


									var quality_message = jsonStr.quality_message;	
									// alert('quality_message --' + quality_message);

									var TotalSUM_base_price = jsonStr.TotalSUM_base_price;
									$("#TotalSUM_base_price_input").val(TotalSUM_base_price);
									//alert('TotalSUM_base_price --' + TotalSUM_base_price);

									var TotalSUM_certification = jsonStr.TotalSUM_certification;
									var TotalSUM_certification_percentage = jsonStr.TotalSUM_certification_percentage;
									if( TotalSUM_certification_percentage == '' ){
										 TotalSUM_certification_percentage = 'N.A.';
									}
									$("#certification_job_input_txt").html( '<span class="totalcost" style="color:#C7E393 !important;" >'+  TotalSUM_certification_percentage + '</span>' );
									
									var is_certification = $('input[name=is_certification]:checked').val();
									$("#certification_job_input").val(is_certification);									
									$("#certification_job_input_name").val('Shancertication'); 
									var certification_input_percent = jsonStr.certification_input_percent;
									$("#certification_input_percent").val(certification_input_percent);
									$("#certification_text").val(TotalSUM_certification_percentage);
									$("#TotalSUM_certification_input").val(TotalSUM_certification);
									//alert('TotalSUM_certification --' + TotalSUM_certification);

									var TotalSUM_domain = jsonStr.TotalSUM_domain;
									var TotalSUM_domain_percentage = jsonStr.TotalSUM_domain_percentage;
									if( TotalSUM_domain_percentage == '' ){
										 TotalSUM_domain_percentage = 'N.A.';
									}
									var domain_id_name = $('#domain_id').find(":selected").text();
									$("#specialised_job_input_txt").html('<span class="totalcost" style="color:#C7E393 !important;" >'+ TotalSUM_domain_percentage + '</span>' );
									var specialised_job_input = $('#domain_id').val();
									$("#specialised_job_input").val(specialised_job_input);									
									$("#specialised_job_input_name").val(domain_id_name);
									var specialised_input_percent = jsonStr.specialised_input_percent;
									$("#specialised_input_percent").val(specialised_input_percent);
									$("#specialised_text").val(TotalSUM_domain_percentage);
									$("#TotalSUM_domain_input").val(TotalSUM_domain);

									//alert('TotalSUM_domain --' + TotalSUM_domain);

									var TotalSUM_quality = jsonStr.TotalSUM_quality;
									var TotalSUM_quality_percentage = jsonStr.TotalSUM_quality_percentage;
									var quality_id_name = $('#quality_id').find(":selected").text();
									if( TotalSUM_quality_percentage == '' ){
										 TotalSUM_quality_percentage = 'N.A.';
									}									
									$("#quality_job_input_txt").html( '<span class="totalcost" style="color:#C7E393 !important;" >'+ TotalSUM_quality_percentage + '</span>' );
									var quality_job_input = $('#quality_id').val();
									$("#quality_job_input").val(quality_job_input);									
									$("#quality_job_input_name").val(quality_id_name);
									var quality_input_percent = jsonStr.quality_input_percent;
									$("#quality_input_percent").val(quality_input_percent);
									$("#quality_text").val(TotalSUM_quality_percentage);
									$("#TotalSUM_quality_input").val(TotalSUM_quality);
									//alert('TotalSUM_quality --' + TotalSUM_quality);


									var TotalProject_cost = jsonStr.TotalProject_cost;
									 $("#NetTotal_input").val(TotalProject_cost);
									 $("#spanCalculating").html(TotalProject_cost);
									 //alert('TotalProject_cost --' + TotalProject_cost);									
									
									
									var penalityFlag = jsonStr.penalityFlag;									
									var penalitySTR = jsonStr.penalitySTR;									
									if( penalityFlag == 1)
									{
										$("#penalty_div").show();
										$("#no_penalty_div").hide();
										$("#penalltySTRID").html(penalitySTR);
										$("#penality_text").html(penalitySTR);
									}else{
										$("#penalty_div").hide();
										$("#no_penalty_div").show();
										$("#penality_text").html('');
									}									
									//alert('penalitySTR --' + penalitySTR);

									var TotalDaysSpend = jsonStr.TotalDaysSpend;
									$("#TotalDaysSpend").html(TotalDaysSpend);
									$("#delivery_day").val(TotalDaysSpend);
									//alert('TotalDaysSpend --' + TotalDaysSpend);
									
									var curr_curency_value = jsonStr.curr_curency_value;


									var currentdate = new Date(); 
									var yearr  = currentdate.getFullYear();
									var monthh  = ("0" + (currentdate.getMonth()+1)).slice(-2);
									var datee   = ("0" + currentdate.getDate()).slice(-2);
									var added_date_time = yearr + '-' + monthh + '-'+ datee + ' ' + currentdate.getHours() + ':' + currentdate.getMinutes() + ':'+currentdate.getSeconds();
									$("#added_date_time").val(added_date_time);


									var target_lang_idSTR = $("#target_lang_id").val();
									$("#target_lang_job_input").val(target_lang_idSTR);
									
    
									//var target_lang_nameSTR = $( "#target_lang_id option:selected" ).text();
									//$("#target_lang_job_input_name").val(target_lang_nameSTR);

									var target_lang_nameSTR = '';
									$countLan = 0;
									$("#target_lang_id option:selected").each(function () {	
										$countLan = $countLan + 1;										
										if( $countLan == 1)
										{
											target_lang_nameSTR =  target_lang_nameSTR + $(this).text();
										}else{
										    target_lang_nameSTR =  target_lang_nameSTR + ' | ' +  $(this).text();
										}	
										
									});
									$("#target_lang_job_input_name").val(target_lang_nameSTR);
									
									
									var minimumBillStrFlag = jsonStr.minimumBillStrFlag;									
									if( minimumBillStrFlag == 'yes' ){
									var minimumBillStr1 = jsonStr.minimumBillSTR;									
									$("#minimumBillStr").html(minimumBillStr1);
									}else{
									$("#minimumBillStr").html('');
									}

									var ratePerUnitSTR = jsonStr.ratePerUnitSTR;
									$("#rate_per_unit_span").html(ratePerUnitSTR);								
									$("#rate_per_unit_text").html(ratePerUnitSTR);

									// Start to paypal
									
									$("#makPay_currency_code").val(currency_code);
									$("#makPay_amount").val(TotalProject_cost);
									
									var service_id_name = $('#service_id').find(":selected").text();
									var service_id_trim =  service_id_name.trim()
									$("#makPay_service_name").val(service_id_trim);

									// End to paypal

									

							}
						});
						
						///////////	

		 }




$(function() {

 



});

</script>