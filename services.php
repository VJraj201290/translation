<?php include("connection.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--  There are 

Following tables are difference in filed with excel 1: tbl_sub_titling_selling | 2: tbl_transcription_selling


<link rel="stylesheet" type="text/css" href="<?php echo site_url; ?>/css/font-awesome.css">
<link href="<?php echo site_url; ?>/css/bootstrap.min1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo site_url; ?>/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo site_url; ?>/js/bootstrap.min.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" />
-->

<link href="<?php echo site_url; ?>/css/left-menu.css" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url; ?>/css/bootstrap.min1.css" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url; ?>/css/left-menu.css" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url; ?>/css/style.css" rel="stylesheet" type="text/css" />

<title>Shantranslation</title>


<script src="js/jquery-1.9.0.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

   /*
	$("#resultsfetch_translation_selling" ).load( "<?php echo site_url; ?>/back_end/fetch_translation_selling.php"); //load initial records
	$(".resultsfetch_translation_selling_loading-div").hide(); //show loading element
	$("#resultsfetch_translation_selling").on( "click", ".pagination a", function (e){
		e.preventDefault();
		$(".resultsfetch_translation_selling_loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#resultsfetch_translation_selling").load("<?php echo site_url; ?>/back_end/fetch_translation_selling.php",{"page":page}, function(){ //get content from PHP page
			$(".resultsfetch_translation_selling_loading-div").hide(); //show loading element
		});		
	});
   */


	$("#resultsfetch_translation_domain" ).load( "<?php echo site_url; ?>/back_end/fetch_translation_domain.php"); //load initial records
	$(".resultsfetch_translation_domain_loading-div").hide(); //show loading element
	$("#resultsfetch_translation_domain").on( "click", ".pagination a", function (e){
		e.preventDefault();
		$(".resultsfetch_translation_domain_loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#resultsfetch_translation_domain").load("<?php echo site_url; ?>/back_end/fetch_translation_domain.php",{"page":page}, function(){ //get content from PHP page
			$(".resultsfetch_translation_domain_loading-div").hide(); //show loading element
		});		
	});



	$("#resultsfetch_transcription_selling" ).load( "<?php echo site_url; ?>/back_end/fetch_transcription_selling.php"); //load initial records
	$(".resultsfetch_transcription_selling_loading-div").hide(); //show loading element
	$("#resultsfetch_transcription_selling").on( "click", ".pagination a", function (e){
		e.preventDefault();
		$(".resultsfetch_transcription_selling_loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#resultsfetch_transcription_selling").load("<?php echo site_url; ?>/back_end/fetch_transcription_selling.php",{"page":page}, function(){ //get content from PHP page
			$(".resultsfetch_transcription_selling_loading-div").hide(); //show loading element
		});		
	});

	$("#resultsfetch_sub_titling_selling" ).load( "<?php echo site_url; ?>/back_end/fetch_sub_titling_selling.php"); //load initial records
	$(".resultsfetch_sub_titling_selling_loading-div").hide(); //show loading element
	$("#resultsfetch_sub_titling_selling").on( "click", ".pagination a", function (e){
		e.preventDefault();
		$(".resultsfetch_sub_titling_selling_loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#resultsfetch_sub_titling_selling").load("<?php echo site_url; ?>/back_end/fetch_sub_titling_selling.php",{"page":page}, function(){ //get content from PHP page
			$(".resultsfetch_sub_titling_selling_loading-div").hide(); //show loading element
		});		
	});

	$("#resultsfetch_proofreading_selling" ).load( "<?php echo site_url; ?>/back_end/fetch_proofreading_selling.php"); //load initial records
	$(".resultsfetch_proofreading_selling_loading-div").hide(); //show loading element
	$("#resultsfetch_proofreading_selling").on( "click", ".pagination a", function (e){
		e.preventDefault();
		$(".resultsfetch_proofreading_selling_loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#resultsfetch_proofreading_selling").load("<?php echo site_url; ?>/back_end/fetch_proofreading_selling.php",{"page":page}, function(){ //get content from PHP page
			$(".resultsfetch_proofreading_selling_loading-div").hide(); //show loading element
		});		
	});

	$("#resultsfetch_typing_selling" ).load( "<?php echo site_url; ?>/back_end/fetch_typing_selling.php"); //load initial records
	$(".resultsfetch_typing_selling_loading-div").hide(); //show loading element
	$("#resultsfetch_typing_selling").on( "click", ".pagination a", function (e){
		e.preventDefault();
		$(".resultsfetch_typing_selling_loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#resultsfetch_typing_selling").load("<?php echo site_url; ?>/back_end/fetch_typing_selling.php",{"page":page}, function(){ //get content from PHP page
			$(".resultsfetch_typing_selling_loading-div").hide(); //show loading element
		});		
	});

});
</script>

<?php

//$_SESSION['message'] = "A BNGH ";
$importTableListArray = array(
         'tableGroup'=> array(
		     'translation'=>array(
			    'selling'=>'selling',
				'domain'=>'domain',
				'quality'=>'quality',
				'penalty'=>'penalty'),
			'transcription'=>array(
			    'selling'=>'selling',
				'domain'=>'domain',
				'quality'=>'quality',
				'penalty'=>'penalty'),
			'sub_titling'=>array(
			    'selling'=>'selling',
				'domain'=>'domain',
				'quality'=>'quality',
				'penalty'=>'penalty'),
			'proofreading'=>array(
			    'selling'=>'selling',
				'domain'=>'domain',
				'quality'=>'quality',
				'penalty'=>'penalty'),
			'typing'=>array(
			    'selling'=>'selling',
				'domain'=>'domain',
				'quality'=>'quality',
				'penalty'=>'penalty'),		 
		 )		 
);


$impFileHeaderArray = array(

		     'selling'=>array('Source_Language','Target_Language',' Price_Per_Word','Price_Per_Page','Minimum_Bill','Shan_Certification_Cost','Words_In_A_Day','Pages_In_A_day','Strength'),
			 'domain'=>array('Domain_Name','Add_Percent_To_Cost','Add_Days'),
			 'quality'=>array('Pro_Efficiency_Level','Add_Percent_To_Cost','Add_Days'),
			 'penalty'=>array('Strength','Penalty_Percent')
);


	$tabFlagTL = 'active';
	$tabFlagTP = '';
	$tabFlagST = '';
	$tabFlagPF = '';
	$tabFlagTY = '';
	
	if( isset($_REQUEST['tabId'])){  
		if((!empty($_REQUEST['tabId']))){
			$tabId = $_REQUEST['tabId'];
			$tabFlagTL = ($tabId == 1) ? 'active':'';
			$tabFlagTP = ($tabId == 2) ? 'active':'';
			$tabFlagST = ($tabId == 3) ? 'active':'';
			$tabFlagPF = ($tabId == 4) ? 'active':'';
			$tabFlagTY = ($tabId == 5) ? 'active':'';
		
		}
    }

  

if( isset($_REQUEST['serviceFlag'])  && ( $_REQUEST['serviceFlag'] == 'yes') ){

//print_r($importTableListArray);
	$tableGroup  = $_REQUEST['tableGroup'];
	$serviceType = $_REQUEST['serviceType'];       
	$tableGroupArray = array('translation'=>1, 'transcription'=>2, 'sub_titling'=> 3, 'proofreading'=>4, 'typing'=>5);	
	$tableGroupSTR = '';
	$serviceTypeSTR = '';	

   foreach($importTableListArray as $key=>$val)
   {  
      
		 if(is_array($val))
		 {
            foreach($val AS $key1=>$val1)
			{

					  if( $key1 == $tableGroup){
						  $tableGroupSTR = $key1;
						  if(is_array($val))
						  {
							  foreach($val1 as $key2=>$val2)
							  {
								 if( $key2 == $serviceType){
									$serviceTypeSTR = $key2;
								 }
							  }
						  }
					  }
			}
		 }
   }

        $import_table_name = 'tbl_'.$tableGroupSTR.'_'.$serviceTypeSTR;
   
   
		$handle = fopen($_FILES['imp_file']['tmp_name'], "r");		
		$i=0;
		$count=0;
		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
		{   $i++;
			
			if( $i == 1)
			{	
				$sizeFileHeaderData = sizeof($data);
				$sizeValidHeaderData = sizeof($impFileHeaderArray[$serviceTypeSTR]);    
				
				
				if($sizeFileHeaderData == $sizeValidHeaderData )
				{  
				   $fileHdFlag = 1;
				   foreach($data as $key => $val ){
					 $comFHVal = strtolower($val);
					 $comVHVal = strtolower( $impFileHeaderArray[$serviceTypeSTR][$key] );  
					 if( $comVHVal != $comVHVal){
						$fileHdFlag = 2;
						$message = " There is invalid file";
						$_SESSION['message'] = $message;
						$siteHead = site_url.'/services.php';
						header("Location: ".$siteHead);
						exit;
					 }
				   }
				     
					  $sqlTrucate = '';
					  if( $fileHdFlag == 1) {
						  $sqlTrucate = "TRUNCATE TABLE {$import_table_name} ";
						  $query = mysql_query($sqlTrucate);
						  
					  }					  				   				  					

				}else{					
					$message =  "Invalid file";
					$_SESSION['message'] = $message;
					$siteHead = site_url.'/services.php';
					header("Location: ".$siteHead);
					exit;
				}
				
				
		   }else
		   {
					if( $serviceTypeSTR == 'domain')
					{
						
						$name = $data[0];
						$add_percent_2 = $data[1];
						$add_days_2 = $data[2];                        
						$sql = "INSERT INTO $import_table_name SET name = '$name', add_percent_2 = '$add_percent_2', add_days_2 = '$add_days_2', created_on = NOW(), status = '1' ";
						$query = mysql_query($sql);
						//$num_rows = mysql_affect_rows($query);
						$count = $count + 1;


					}elseif($serviceTypeSTR == 'penalty'){
						$strength = $data[0];
						$penalty_percent = $data[1];						                        
						$sql = "INSERT INTO $import_table_name SET strength = '$strength', penalty_percent = '$penalty_percent', created_on = NOW(), status = '1' ";
						$query = mysql_query($sql);
						//$num_rows = mysql_affect_rows($query);
						$count = $count + 1;

					}elseif($serviceTypeSTR == 'quality'){
						$pro_level = $data[0];
						$add_percent_2 = $data[1];
						$add_days_2 = $data[2];
						$sql = "INSERT INTO $import_table_name SET pro_level = '$pro_level', add_percent_2 = '$add_percent_2', add_days_2 = '$add_days_2', created_on = NOW(), status = '1' ";
						$query = mysql_query($sql);
						//$num_rows = mysql_affect_rows($query);
						$count = $count + 1;
					}elseif($serviceTypeSTR == 'selling'){
						
						if( ($import_table_name == 'tbl_sub_titling_selling') || ( $import_table_name == 'tbl_transcription_selling' ) ){
						
							$source_language = $data[0];
							$target_language = $data[1];
							$price_per_minute = $data[2];
							$price_per_hour = $data[3];
							$minimum_bill = $data[4];
							$shan_certification = $data[5];
							$minutes_in_a_day = $data[6];
							$hours_in_a_day = $data[7];
							$strength = $data[8];

							$sql = "INSERT INTO $import_table_name SET source_language = '$source_language', target_language = '$target_language', price_per_minute = '$price_per_minute', price_per_hour = '$price_per_hour', minimum_bill = '$minimum_bill', shan_certification = '$shan_certification', minutes_in_a_day = '$minutes_in_a_day', hours_in_a_day = '$hours_in_a_day', strength = '$strength', created_on = NOW(), status = '1' ";
							$query = mysql_query($sql);
							//$num_rows = mysql_affect_rows($query);
							$count = $count + 1;
						}else if( ($import_table_name == 'tbl_proofreading_selling') || ( $import_table_name == 'tbl_translation_selling' )  || ($import_table_name == 'tbl_typing_selling') ){
						
							$source_language = $data[0];
							$target_language = $data[1];
							$price_per_word = $data[2];
							$price_per_page = $data[3];
							$minimum_bill = $data[4];
							$shan_certification = $data[5];
							$words_in_a_day = $data[6];
							$pages_in_a_day = $data[7];
							$strength = $data[8];

							$sql = "INSERT INTO $import_table_name SET source_language = '$source_language', target_language = '$target_language', price_per_word = '$price_per_word', price_per_page = '$price_per_page', minimum_bill = '$minimum_bill', shan_certification = '$shan_certification', words_in_a_day = '$words_in_a_day', pages_in_a_day = '$pages_in_a_day', strength = '$strength', created_on = NOW(), status = '1' ";
							$query = mysql_query($sql);
							//$num_rows = mysql_affect_rows($query);
							$count = $count + 1;						

						}

					}		        
               
		   }

		  
		}


        $tabID = $tableGroupArray[$tableGroup];
	
		$message = "<br/><br/><div class='success' > {$count} records inserted successfully. </div>";
		$_SESSION['message'] = $message;
		$siteHead = site_url.'/services.php?tabId='.$tabID;
		header("Location: ".$siteHead);
		exit;
	
}

?>

</head>

<body>
<?php  require_once(ABSPATH.'front_end/shan_trans_header.php'); ?>
    <section>
    	<div class="container-fluid">
        	<div class="row">
        	<div class="mid-sec">
                    <div class="col-sm-3 col-md-2">
                    <!-- Start to nav menu -->
					<?php  require_once(ABSPATH.'menu_bar.php'); ?>
					<!-- End to nav menu -->
                    </div>
					 <!-- Start to right section page content  -->
					<div class="col-md-10 pull-right pagebottom" >
					     <div class="service-hed" style="margin-top:0px !important;">Service Management </div><br/>
						 <?php if(isset($_SESSION['message'])){ echo $_SESSION['message']; echo "<br/>"; unset($_SESSION['message']);  } ?>
					     <?php  require_once(ABSPATH.'page_container/services_content.php'); ?>
					</div>
					<!-- End to right section page content  -->                    
            </div>
            </div>
        </div>
    </section>
    
	<?php  require_once(ABSPATH.'front_end/shan_trans_footer.php'); ?>
    
</body>


<!-----for tab2----->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/master.js"></script>


<link href="css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.12.4.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function() {

    $('#tbl_translation_selling_grid').DataTable();
	$('#tbl_translation_domain_grid').DataTable();
	$('#tbl_translation_quality_grid').DataTable();
	$('#tbl_translation_penality_grid').DataTable();

    $('#tbl_transcription_selling_grid').DataTable();
	$('#tbl_transcription_domain_grid').DataTable();
	$('#tbl_transcription_quality_grid').DataTable();
	$('#tbl_transcription_penality_grid').DataTable();

    $('#tbl_sub_titling_selling_grid').DataTable();
	$('#tbl_sub_titling_domain_grid').DataTable();
	$('#tbl_sub_titling_quality_grid').DataTable();
	$('#tbl_sub_titling_penality_grid').DataTable();

    $('#tbl_proofreading_selling_grid').DataTable();
	$('#tbl_proofreading_domain_grid').DataTable();
	$('#tbl_proofreading_quality_grid').DataTable();
	$('#tbl_proofreading_penality_grid').DataTable();

    $('#tbl_typing_selling_grid').DataTable();
	$('#tbl_typing_domain_grid').DataTable();
	$('#tbl_typing_quality_grid').DataTable();
	$('#tbl_typing_penality_grid').DataTable();


	
} );
</script>
<!-----for tab2----->

</html>
