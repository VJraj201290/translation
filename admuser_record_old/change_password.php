<?php include("../connection.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<?php	
	if(!isset($_SESSION['SESS_id']))
	{
		$siteHead = site_url.'/index.php';
		header("Location: ".$siteHead);
		exit;
	}else if( $_SESSION['SESS_user_type'] != 'A'){

		$siteHead = site_url.'/index.php';
		header("Location: ".$siteHead);
		exit;
	
	}
	
	
	if(!empty($_REQUEST['deleteUser']) && ( $_REQUEST['deleteUser'] == 'yes') ){
		if( !empty($_REQUEST['userList']) && sizeof( $_REQUEST['userList']) > 0 ){
			
			$idImplode = implode(",",$_REQUEST['userList']);
			$query = "UPDATE tbl_user SET status = 'D'  WHERE id IN ( $idImplode )  ";			
			$result = mysql_query($query);
			
			if(mysql_affected_rows() >0){						  				
				$message = " Delete successfully  ";
				$_SESSION['message'] = $message;
			}else{
				$message = "Sorry! could not delete, try latter.";
				$_SESSION['message'] = $message; 
			}			
			
		}else{
		  
		}
	
	}


?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?php echo site_url; ?>/css/left-menu.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url; ?>/css/font-awesome.css">
<link href="<?php echo site_url; ?>/css/bootstrap.min1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo site_url; ?>/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo site_url; ?>/js/bootstrap.min.js"></script>
<link href="<?php echo site_url; ?>/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url; ?>/css/tooltip.css" rel="stylesheet" type="text/css" />
<title>Shantranslation</title>
</head>

<body>
<?php  require_once(ABSPATH.'front_end/shan_trans_header.php'); ?>
    <section>
    	<div class="container-fluid">
        	<div class="row">
        	<div class="mid-sec">
                    <div class="col-sm-3 col-md-2">
                    <!-- Start to nav menu -->
					<?php  require_once(ABSPATH.'menu_bar.php'); ?>
					<!-- End to nav menu -->
                    </div>
					 <!-- Start to right section page content  -->
					<div class="col-md-10 pull-right pagebottom" >
					     <div class="service-hed" style="margin-top:0px !important;">Change Password</div><br/>
						 <?php if(isset($_SESSION['message'])){ echo $_SESSION['message']; echo "<br/>"; unset($_SESSION['message']);  } ?>
					     <!-- Start to page content  --->
							  <div class="tab-pane fade <?php echo $changeActive; ?>" id="tab5danger"> 
							   <form method="post" id="changePassfrm" > 
								<div class="change-pass-main"> <div id="changePasswordMess" ></div>
								  <div class="change-pass1">Change Password</div>
								  <div class="change-pass12" style="min-height:250px;" >
									<input type="password" class="form-control" name="old_Password" id="oldPassword" placeholder="Old Password"><BR style="clear:both;" >
									<input type="password" class="form-control" name="new_password" id="newPassword1" placeholder="New Password"><BR style="clear:both;" >
									<input type="password" class="form-control" name="confirm_password" id="newPassword2" placeholder="Reconfirm New Password">
									<input type="hidden" class="form-control" name="pass_module_user_id" id="pass_module_user_id" value="<?php echo $_SESSION['SESS_id']; ?>" />
									<input type="hidden" class="form-control" name="changePassword" id="changePassword" value="yes" >
									<button type="button" id="chanPass" class="btn view-order-btn" style="margin-left:250px;" >Submit</button>
								  </div>
								  
								</div>
								</form>  
								
								
							  </div>
						 <!--- End to page content---->
					</div>
					<!-- End to right section page content  -->                    
            </div>
            </div>
        </div>
    </section>
    
	<?php  require_once(ABSPATH.'front_end/shan_trans_footer.php'); ?>


<script type="text/javascript" >
		   


			$(document).ready(function(){
				
				 $("#chanPass").click(function(){	
					 
						var errFlag = 0;
						var oldPassword = $("#oldPassword").val();
						var newPassword1 = $("#newPassword1").val();
						var newPassword2 = $("#newPassword2").val();
						var mess = 'Enter following \n \n';
						if(oldPassword == ''){								
							mess = mess + 'Old password. \n';
							errFlag = 1;	
						}

						if(newPassword1 == ''){								
							mess = mess + 'New password. \n';	
							errFlag = 1;	
						}
					
						if(newPassword2 == ''){								
							mess = mess + 'Confirm password. \n';	
							errFlag = 1;	
						}

						if( (newPassword1 != '') && ( newPassword2 != '' )){
						   if( newPassword1 != newPassword2 ){
							mess = mess + 'Password and reconfirm password. \n';	
							errFlag = 1;						   
						   }
						}

						if( errFlag == 1 ){
						  alert(mess);
						  return false;
						}

					var sub_url = '<?php echo site_url; ?>';
					var urlSel = sub_url + "/user_record/ajax_change_password.php";														    

					$.ajax({
						url: urlSel, 
						type : "POST", 
						dataType : 'json', 
						data : $("#changePassfrm").serialize(), 
						success : function(jsonStr) {
							var message = 	jsonStr.message;	   
							$("#changePasswordMess").html(message);				 													  
						},
						error: function(xhr, resp, text) {
							console.log(xhr, resp, text);
						}
					})
					return false;
                   
				});
			});

</script>
    
</body>


<!-----for tab2----->

</html>
