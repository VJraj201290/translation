<?php
/* Title : Ajax Pagination with jQuery & PHP
Example URL : http://www.sanwebe.com/2013/03/ajax-pagination-with-jquery-php */

//continue only if $_POST is set and it is a Ajax request
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
	
	include("../connection.php");
	
	//include config file
	//Get page number from Ajax POST
	if(isset($_POST["page"])){
		$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
	}else{
		$page_number = 1; //if there's no page number, set it to 1
	}
	
	//get total number of records from database for pagination
	$results = $mysqli->query("SELECT COUNT(*) FROM tbl_typing_selling");
	$get_total_rows = $results->fetch_row(); //hold total records in variable
	//break records into pages
	$total_pages = ceil($get_total_rows[0]/$item_per_page);
	
	//get starting position to fetch the records
	$page_position = (($page_number-1) * $item_per_page);
	

	//Limit our results within a specified range. 
	$results = $mysqli->prepare("SELECT id, source_language, target_language, price_per_word, price_per_page, minimum_bill, shan_certification, words_in_a_day, pages_in_a_day, strength FROM tbl_typing_selling ORDER BY id ASC LIMIT $page_position, $item_per_page");
	$results->execute(); //Execute prepared Query
	$results->bind_result( $id, $source_language, $target_language, $price_per_word, $price_per_page, $minimum_bill, $shan_certification, $words_in_a_day, $pages_in_a_day, $strength ); //bind variables to prepared statement
	
	//Display records fetched from database.
	?>


		<table class="table table-hover entries"> 
		<thead>
		<tr>
			<th>S. N.</th> 
			<th>Source Language</th>
			<th>Target Language </th> 
			<th>Price/Minute</th>
			<th>Price/Hour</th>
			<th>Minimum Bill </th>
			<th>Certification</th>
			<th>Minutes/Day</th>
			<th>Hours/Day</th>
			<th>Strength</th>
			
	   </tr>
		</thead>
        <tbody>
	<?php

	 $statusArray = array(0=>'Inactive', 1=>'Active');
	while($results->fetch()){ //fetch values
		?>

		  <tr> 
			  <th scope="row"><?php echo $id; ?></th> 
			  <td><?php echo $source_language; ?></td> 
			  <td><?php echo $target_language; ?></td>
			  <td><?php echo $price_per_word; ?></td>
			  <td><?php echo $price_per_page; ?></td> 
			  <td><?php echo $price_per_page; ?></td>
			  <td><?php echo $shan_certification; ?></td>
			  <td><?php echo $words_in_a_day; ?></td> 
			  <td><?php echo $pages_in_a_day; ?></td>
			  <td><?php echo $strength; ?></td>
			  
			  
		  </tr>

<?php	} ?>
		</tbody> 
	 </table>                                         
<?php

	echo '<div align="center">';
	/* We call the pagination function here to generate Pagination link for us. 
	As you can see I have passed several parameters to the function. */
	echo paginate_function($item_per_page, $page_number, $get_total_rows[0], $total_pages);
	echo '</div>';
	
	exit;
}
################ pagination function #########################################
function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
{
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
        $pagination .= '<ul class="pagination">';
        
        $right_links    = $current_page + 3; 
        $previous       = $current_page - 3; //previous link 
        $next           = $current_page + 1; //next link
        $first_link     = true; //boolean var to decide our first link
        
        if($current_page > 1){
			$previous_link = ($previous==0)? 1: $previous;
            $pagination .= '<li class="first"><a href="#" data-page="1" title="First">&laquo;</a></li>'; //first link
            $pagination .= '<li><a href="#" data-page="'.$previous_link.'" title="Previous">&lt;</a></li>'; //previous link
                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
                    if($i > 0){
                        $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
                    }
                }   
            $first_link = false; //set first link to false
        }
        
        if($first_link){ //if current active page is first link
            $pagination .= '<li class="first"><a href="#">'.$current_page.'</a></li>';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="last"><a href="#" >'.$current_page.'</a></li>';
        }else{ //regular current link
             $pagination .= '<li class=""><a href="#" >'.$current_page.'</a></li>';
        }
                
        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
               $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
            }
        }
        if($current_page < $total_pages){ 
				$next_link = ($i > $total_pages) ? $total_pages : $i;
                $pagination .= '<li><a href="#" data-page="'.$next_link.'" title="Next">&gt;</a></li>'; //next link
                $pagination .= '<li class="last"><a href="#" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
        }
        
        $pagination .= '</ul>'; 
    }
    return $pagination; //return pagination links
}

?>

