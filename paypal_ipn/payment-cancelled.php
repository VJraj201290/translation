<?php include("../connection.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php	

	
	if(!isset($_SESSION['SESS_id']))
	{
		$siteHead = site_url.'/index.php';
		header("Location: ".$siteHead);
		exit;

		
	}

	$user_id = $_SESSION['SESS_id'];
	
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?php echo site_url; ?>/css/bootstrap.min1.css" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url; ?>/css/left-menu.css" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url; ?>/css/style.css" rel="stylesheet" type="text/css" />
<title>Shantranslation Payment Cancelled</title>
</head>

<body>
<?php  require_once(ABSPATH.'front_end/shan_trans_header.php'); ?>
<section>
  <div class="container-fluid">
    <div class="row">
      <div class="mid-sec"> 
        
        <!-- dashboard start-->
        <div class="dashboard-main">
          <div class="dash1">
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 pull-right">
              <!--<button class="dash-btn">Translation Management</button>-->
              <a href="<?php echo site_url; ?>/user_record.php" ><button class="dash-btn actdash">Dashboard</button></a>
			  <a href="<?php echo site_url; ?>/logout.php" ><button class="dash-btn">Logout</button></a>
            </div>
          </div>
          <div class="dash2">
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 pull-right">
              <div class="dash-text">Dashboard</div> <div class="dash-text" style="float:right;margin-right:20%;" >Welcome! <span class="user_dash" >&nbsp;&nbsp;&nbsp;<?php echo $_SESSION['SESS_name']; ?></span></div>
            </div>
          </div>
        </div>
        <!-- dashboard end--> 
        
 
        <div class="col-md-10 pull-right" style="padding-bottom:120px;" > 
          <!--<div class="service-hed">Service Management</div>-->
          <div class="mid-sec1">
            <div class="panel with-nav-tabs1 panel-danger1">
              
              <div class="panel-body">
                <div class="tab-content" > 	
				    <?php //print_r( $_REQUEST); ?>
					<h1>Payment Cancelled</h1>
					<p>Your payment was cancelled.</p>     				
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php  require_once(ABSPATH.'front_end/shan_trans_footer.php'); ?>
</body>

<!-----for tab2----->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/master.js"></script>
<link href="css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.12.4.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function() {
    $('#example').DataTable();
	$('#example1').DataTable();
	$('#example2').DataTable();
	$('#example3').DataTable();
	$('#example4').DataTable();
	$('#example5').DataTable();
	$('#example6').DataTable();
	$('#example7').DataTable();
	$('#example8').DataTable();
	$('#example9').DataTable();
	$('#example10').DataTable();
	$('#example11').DataTable();
	$('#example12').DataTable();
	$('#example13').DataTable();
	$('#example14').DataTable();
	$('#example15').DataTable();
	$('#example16').DataTable();
	$('#example17').DataTable();
	$('#example18').DataTable();
	$('#example19').DataTable();
	
} );
</script>
</html>
