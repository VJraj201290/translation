<?php 

$HostName = $_SERVER['HTTP_HOST'];
if($HostName == 'localhost' ){
	 require_once("../connection.php"); 
}else{
	 require_once("../connection.php"); 
}
 
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php	

	
	if(!isset($_SESSION['SESS_id']))
	{
		$siteHead = site_url.'/index.php';
		header("Location: ".$siteHead);
		exit;

		
	}

	$user_id = $_SESSION['SESS_id'];
	
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?php echo site_url; ?>/css/bootstrap.min1.css" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url; ?>/css/left-menu.css" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo site_url; ?>/css/style.css" rel="stylesheet" type="text/css" />
<title>Shantranslation Payment Successful</title>
</head>

<body>
<?php  require_once(ABSPATH.'front_end/shan_trans_header.php'); ?>
<section>
  <div class="container-fluid">
    <div class="row">
      <div class="mid-sec"> 
        
        <!-- dashboard start-->
        <div class="dashboard-main">
          <div class="dash1">
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 pull-right">
              <button class="dash-btn">Translation Management</button>
              <button class="dash-btn actdash">Dashboard</button>
            </div>
          </div>
          <div class="dash2">
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 pull-right">
              <div class="dash-text">Dashboard</div>
            </div>
          </div>
        </div>
        <!-- dashboard end--> 
        
 
        <div class="col-md-10 pull-right" style="padding-bottom:120px;" > 
          <!--<div class="service-hed">Service Management</div>-->
          <div class="mid-sec1">
            <div class="panel with-nav-tabs1 panel-danger1">
              
              <div class="panel-body">
                <div class="tab-content" > 
				<?php 
				//print_r( $_REQUEST);
				//echo '<BR /><BR />-----------------------------<BR /><BR />'; 
				$item_number = $_REQUEST['item_number'];				
				$item_numberArray = explode('___',$item_number);
				$order_id = $item_numberArray[1];
				$txnid = $_REQUEST['txn_id'];
				$payment_amount = $_REQUEST['payment_gross'];
				$payment_status = $_REQUEST['payment_status'];
				$itemid = $_REQUEST['item_number'];
                
				$order_mess = '<p>Update successfully.</p>';
			    if( $payment_status == 'Completed' ){
                  $order_status_id = 6;
				  $order_mess = '<p>Your payment was successful.</p>';
				}else if( $payment_status == 'Pending' ) {
				  $order_status_id = 7;
				  $order_mess = '<p>Your payment is pending.</p>';
				}
            
			   if( isset($_REQUEST['txn_type'])  && isset($_REQUEST['txn_id']) && ( $order_id > 0 )  ){
			  
							   $sqlCheck = "SELECT  id from tbl_paypal_payments WHERE txnid = '$txnid' ";
							   $result = mysql_query($sqlCheck);
							   $num_rows = mysql_num_rows($result);

							   if($num_rows > 0 ){
										 echo '<h1>Thank you.</h1>';
										 echo '<p>Already your order is updated.</p> ';								
							   }else{
								  
								   $sqlInser = "INSERT INTO tbl_paypal_payments SET txnid = '$txnid', payment_amount = '$payment_amount', payment_status = '$payment_status', itemid = '$itemid', order_id = '$order_id', createdtime = NOW()  "; 
								   $result = mysql_query($sqlInser);				  
								   $num_rows = mysql_affected_rows(); 
								   if( mysql_affected_rows() > 0){
										$last_reg_Id = mysql_insert_id();

										if( $last_reg_Id > 0 ){

											// Strat to order table
											$sqlInser1 = "UPDATE tbl_order SET order_status_id = '$order_status_id'  WHERE order_id = '$order_id'  ";
											$result = mysql_query($sqlInser1);
											
											echo '<h1>Thank You</h1>';
											echo $order_mess;

										}else{						
										 echo '<h1></h1>';
										 echo $order_mess;

										} 

									 
								   }else{
									 
										 echo '<h1></h1>';
										 echo '<p>Sorry, Order could not update, contact to admin.</p> ';

								   }
							   }
				   }else{
				   
										 echo '<h1></h1>';
										 echo '<p>There is invalid parameter.</p> ';				   
				   }

				?>
                                 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php  require_once(ABSPATH.'front_end/shan_trans_footer.php'); ?>
</body>

<!-----for tab2----->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/master.js"></script>
<link href="css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.12.4.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function() {
    $('#example').DataTable();
	$('#example1').DataTable();
	$('#example2').DataTable();
	$('#example3').DataTable();
	$('#example4').DataTable();
	$('#example5').DataTable();
	$('#example6').DataTable();
	$('#example7').DataTable();
	$('#example8').DataTable();
	$('#example9').DataTable();
	$('#example10').DataTable();
	$('#example11').DataTable();
	$('#example12').DataTable();
	$('#example13').DataTable();
	$('#example14').DataTable();
	$('#example15').DataTable();
	$('#example16').DataTable();
	$('#example17').DataTable();
	$('#example18').DataTable();
	$('#example19').DataTable();
	
} );
</script>
</html>
